# Huntserv
The server for a treasure hunt app.
A running server is available at `hunt.xa1.uk:25255`, and a minimal interface
at (`https://hunt.xa1.uk/console.html`)[`https://hunt.xa1.uk/console.html`].

Generated documentation: https://xa1.uk/docs/huntserv/

## Repo Structure

 - *protocol* : A link to the repo containing the protcol definition.
 - *src* : The main source code
   - src/server : Source code for the server binary
   - src/huntdb : Creates a test database from some data defined in JSON.
   - src/fake_players : Connects with many players simultaniously, used in testing.
 - *deploy* : Various scripts for creating and running a huntserv docker image.
 - Cargo.toml : The build file for cargo, rust's dependency manager & build tool.
 - .env : An example configuration file.
 - test_identity.pfx : A self-signed identity file for testing.
 - data.json : Used by `huntdb` to generate a database for testing.

## Initialising a database

The protocol contains functions for adding hunts, clues, players, etc, but in
some circumstances it may be easier to create a database with a treasure hunt
already set up and ready to start. The `huntdb` tool reads `data.json` and
writes a new database to the `db` folder with all the data defined in the JSON
file. This has been used to create the database used in most of our testing.

Usernames are generated based on the name, the format is the first initial followed
by the last name, followed by a number to disambiguate, all in lowercase.

When `random_passwords` is true, passwords will be randomly generated 6 character
alphanumeric strings, usernames and passwords are printed to the console next
to the player's full name -- note that only password hashes are stored, so it's
not possible to get the passwords again afterwards. When `random_passwords` is
false, "password" is used for all players, useful for testing ^^

## Deployment

The server is deployed as a docker image, so can run on any linux server with docker.
The image is available on the docker hub as `xalri:huntserv`.

`huntserv` uses TLS for security, so a valid SSL key and certificate must be
available on the host. The server expects this as a PKCS #12 archive, which can
be generated using openssl:

```
openssl pkcs12 -export -out identity.pfx -inkey key.pem -in cert.pem -certfile cert_chain.pem
```

The archive is passed to the container through a volume, and the path and password
are given as environmental variables.

We use a simple file-backed database, to store this data outside the container, set
a path for the `/db` volume as shown below.

The following command starts the container, exposing the required ports to the host.

```
docker run \
    --name huntserv \
    -v /path/to/certs/:/certs \
    -v /path/to/database/:/db/ \
    -e KEY_ARCHIVE=/certs/identity.pfx \
    -e KEY_ARCHIVE_PASSWORD=hunter2 \
    -e ADMIN_PASSWORD=f67206f353 \
    -p 0.0.0.0::25255 \
    -p 0.0.0.0::25256 \
    -d xalri:huntserv
```

In this example, the directory `/path/to/certs/` should contain a file `identity.pfx`.

When set, `ADMIN_PASSWORD` gives the password to use for an admin account with
the username `admin`, which can be used to add new accounts, and disabled when
it's no longer needed by restarting the server with that value un-set.

## Building the image

It's also possible to build the image directly, rather than using the pre-built
image on the docker hub:

Building the server image requires a nightly rust compiler (last tested with
rustc 1.18.0 2017-04-05) and a running instance of docker.

 - Move into the `deploy` directory
 - Run `build.sh` to build the image, A file named `huntserv.tar` will be
   created, which can be moved to the server and loaded into docker with
   `docker load -i huntserv.tar`
 - Note that debian stretch (The base image for this container) uses the new
   OpenSSL 1.1 by default, if you're building on a system which only has 1.0,
   add `libssl1.0.2` to the end of the second line of the dockerfile.
