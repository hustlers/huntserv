#!/bin/bash
set -e

cd ../
cargo build --release
cd deploy

cp ../target/release/server ./
docker build -t xalri/huntserv .
docker save -o huntserv.tar xalri/huntserv
