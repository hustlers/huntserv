set -e

./build.sh
rsync -z --progress ./huntserv.tar xa1.uk:./
ssh xa1.uk "bash -s" <<< "
   docker rm -f huntserv;
   docker rmi xalri/huntserv;
   docker load -i ./huntserv.tar &&
   echo 'Starting container' &&
   docker run \
       --name huntserv \
       -v /home/xal/hunt_db/:/db/ \
       -v /home/xal/certs/:/certs/ \
       -e KEY_ARCHIVE=/certs/hunt.pfx \
       -e KEY_ARCHIVE_PASSWORD=hunter2 \
       -e ADMIN_PASSWORD=password \
       -p 0.0.0.0:25255:25255 \
       -p 0.0.0.0:25256:25256 \
       -d xalri/huntserv"
