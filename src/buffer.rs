
/// A fixed size circular buffer used for keeping copies of old data where it
/// might be needed again soon.
#[derive(Debug)]
pub struct Buffer<T> {
   data: Vec<T>,
   /// The capacity of the buffer
   capacity: u64,
   /// The current position in the buffer
   pos: u64,
   /// The logical index of the oldest element of the buffer, equivalent to the
   /// number of items which have been lost.
   overflow: u64,
}

impl<T> Buffer<T> {
   /// Create a new buffer with the given capacity
   pub fn with_capacity(size: u64) -> Buffer<T> {
      Buffer {
         data: Vec::with_capacity(size as usize),
         capacity: size,
         pos: 0,
         overflow: 0,
      }
   }

   /// Add a value to the end of the buffer
   pub fn add(&mut self, value: T) {
      if self.data.len() < self.capacity as usize {
         self.data.push(value);
         return;
      }
      if self.pos == self.capacity {
         self.pos = 0
      }
      self.overflow += 1;
      self.data[self.pos as usize] = value;
      self.pos += 1;
   }

   /// Get a value from the buffer. Returns `None` if the value is out of range.
   pub fn get(&self, index: u64) -> Option<&T> {
      if index > self.overflow + self.data.len() as u64 - 1 {
         return None;
      }
      if index < self.overflow {
         return None;
      }
      Some(&self.data[(index % self.capacity) as usize])
   }
}

#[test]
fn buffer() {
   let mut b = Buffer::with_capacity(4);
   for i in 0..6 {
      b.add(i);
   }
   assert_eq!(None, b.get(6));
   assert_eq!(Some(&5), b.get(5));
   assert_eq!(Some(&2), b.get(2));
   assert_eq!(None, b.get(1));

   for i in 0..6 {
      b.add(i);
   }
   assert_eq!(None, b.get(7));
   assert_eq!(None, b.get(12));
   assert_eq!(Some(&5), b.get(11));
   assert_eq!(Some(&2), b.get(8));
}
