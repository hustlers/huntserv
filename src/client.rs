use data::*;
use error::*;
use game::*;
use connection::*;
use database::Database;
use listen::*;
use message::*;
use test_data;

use futures::sync::mpsc::*;
use rand;
use serde_json;
use chrono::UTC;
use protobuf::RepeatedField;
use fnv::FnvHashSet as HashSet;
use fnv::FnvHashMap as HashMap;
use std::time::*;
use std::cmp;
use std::sync::Arc;

/// The number of messages to keep in a chat group, old messages are deleted.
const MAX_MESSAGE_HISTORY: usize = 50;

/// The maximum number of updates to send at a time.
const MAX_SEND_GROUP: usize = 200;

/// The priority of some data or notification.
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
pub enum Priority {
   /// A high priority message, sent even when the user's status is set to asleep.
   High,
   /// A low priority.
   Low
}

/// The ID to use for different kinds of notifications.
/// Only one notification of the same type will be displayed at the same time,
/// the most recent will overwrite older ones.
pub enum NotifyID {
   /// For testing notifications.
   Test = 25255,
   /// A new clue is available.
   NewClue = 25256,
   /// A bonus point has been found.
   BonusUnlock = 25257,
   /// The team has completed the hunt.
   HuntComplete = 25258,
   /// A message has been received.
   NewMessage = 25260,
}

/// A single client connected to the server.
pub struct Client {
   log: ::Log,
   /// The user this client is logged in as.
   user: User,
   /// The connection to the client.
   connection: Connection,
   session_id: u128,
   /// A shared reference to the main data store.
   db: Arc<Database>,
   /// A queue of updates we're sending to the client.
   out_queue: Vec<Update>,
   /// Set of data IDs for which we have queued data, used to prevent duplicate
   /// updates.
   update_set: HashSet<(DataType, u32)>,
   /// The time at which the last ping was sent.
   ping_sent: Option<Instant>,
   /// The game we're currently participating in.
   game: Option<SharedGame>,
   /// A channel on which to send events to other clients.
   ev_snd: UnboundedSender<GameEv>,
   /// A map of data we have recently added to the request ID, so we can set
   /// the request ID in the response.
   data_add_map: HashMap<(DataType, u32), u32>
}

#[derive(Debug)]
enum Update {
   Data{ priority: Priority, data: DataInner, req_id: Option<u32> },
   Notification{ priority: Priority, timeout: Option<Instant>, data: Notification},
}

impl Client {
   /// Create a new client.
   pub fn new(log: ::Log, user: User, connection: Connection, db: Arc<Database>,
              ev_snd: UnboundedSender<GameEv>) -> Client
   {
      Client {
         log, user, connection, db, ev_snd,
         ping_sent: None,
         session_id: rand::random(),
         out_queue: Vec::new(),
         update_set: HashSet::default(),
         game: None,
         data_add_map: HashMap::default(),
      }
   }

   /// Send the initial data to the client.
   pub fn init(&mut self, mut listen: Listens) {
      let user_id = self.user_id();

      if self.permission() == Permission::ADMIN {
         for hunt_id in self.db.hunts.id_set() {
            let res = self.follow_tree(DataType::HUNT, hunt_id, &mut listen);
            if let Err(e) = res {
               error!(self.log, "Failed to send hunt info to an admin: {}", e);
            }
         }
      }

      let res = self.follow_tree(DataType::USER, user_id, &mut listen);
      if let Err(e) = res {
         error!(self.log, "client.init failed! This client may be missing some data: {}", e);
      }
      self.user.set_status(PlayerStatus::ACTIVE);
      self.write_user();
      self.send_ev(GameEv::DataUpdate(DataType::USER, self.user_id()));
   }

   /// Tell the client we have finished sending the intial data.
   pub fn login_complete(&mut self) -> u128 {
      let mut message = Message::new();

      let mut session = Session::new();
      session.set_user_id(self.user_id());
      session.set_session_id(format!("{:032X}", self.session_id));
      message.set_login_complete(session);
      self.connection.send(message);

      self.session_id
   }

   /// Handle an incoming message from the client.
   pub fn handle(&mut self, msg: Message) {
      if let Some(data) = self.connection.handle(msg) {
         match data {
            MessageData::ping(ping) => self.handle_ping(ping),
            MessageData::data(data) => self.data_update(data),
            MessageData::data_add(data) => self.data_add(data),
            MessageData::unlock(code) => self.unlock(code),
            MessageData::notify(x) => self.handle_notify(x),
            MessageData::data_delete(ids) => self.data_delete(ids),
            MessageData::hunt_start(id) => self.hunt_start(id),
            MessageData::json_hunt(json) => self.json_hunt(json),
            x => warn!(self.log, "Got unexpected message: {:?}", x),
         }
      }
   }

   /// Ping the client.
   pub fn ping(&mut self) {
      if self.ping_sent.is_none() {
         let mut reply = Message::new();
         reply.set_ping(true);
         self.connection.send(reply);
         self.ping_sent = Some(Instant::now());
      }
   }

   /// Attempt to join a game.
   pub fn join_game(&mut self, game: SharedGame) {
      self.game = Some(game);
   }

   /// Get the permission level of this user.
   pub fn permission(&self) -> Permission { self.user.get_permission() }

   /// Get the current state of the user.
   pub fn state(&self) -> PlayerStatus { self.user.get_status() }

   /// Get the ID of the current connection.
   pub fn id(&self) -> u64 { self.connection.id() }

   /// Get the ID of the user the client is logged in as.
   pub fn user_id(&self) -> u32 { self.user.get_id() }

   /// Get the client's session ID
   pub fn session_id(&self) -> u128  { self.session_id }

   /// Get the ID of the hunt this user is a part of.
   pub fn hunt_id(&self) -> Option<u32> {
      self.db.teams.get(self.user.get_team_id())
         .map(|team| team.get_hunt())
   }

   /// Borrow the `Connection` to the client.
   pub fn connection(&self) -> &Connection {
      &self.connection
   }

   /// Set the user's status to offline.
   pub fn disconnected(&mut self) {
      self.user.set_status(PlayerStatus::OFFLINE);
      self.write_user();
      self.send_ev(GameEv::DataUpdate(DataType::USER, self.user_id()));
   }

   /// Attempt to resume with a new connection, returns the connection if it fails.
   pub fn resume(&mut self, mut new: Connection) -> Option<Connection> {
      if !self.connection.resume(&mut new) { return Some(new); }
      self.connection.shutdown();
      self.connection = new;
      self.send_updates();
      self.login_complete();
      None
   }

   /// To be called when the server is shutting down.
   pub fn shutdown(&mut self) {
      self.user.set_status(PlayerStatus::OFFLINE);
      self.write_user();
   }

   /// Send queued updates to the client.
   pub fn send_updates(&mut self) {
      let now = Instant::now();

      let mut i = self.out_queue.len();
      let mut sent = 0;
      while i > 0 && sent < MAX_SEND_GROUP {
         i -= 1;
         let priority = match self.out_queue[i] {
            Update::Data{ priority, .. } | Update::Notification{ priority, .. } => priority,
         };

         if self.should_send(priority) {
            let update = self.out_queue.remove(i);
            match update {
               Update::Data{ data, req_id, .. } => {
                  self.update_set.remove(&(data.kind(), data.id()));
                  let mut msg = data.into_msg();
                  if let Some(seq) = req_id {
                     msg.set_request_seq(seq);
                  }
                  self.connection.send(msg);
                  sent += 1;
               }
               Update::Notification{ timeout, data, .. } => {
                  let to_send = if let Some(time) = timeout { time > now }
                  else { true };

                  if to_send {
                     let mut msg = Message::new();
                     msg.set_notify(data);
                     self.connection.send_with_timeout(msg, timeout);
                     sent += 1;
                  }
               }
            }
         }
      }
   }

   /// Follow the tree of the given item, adding each associated item to our
   /// listen set and the given listen map, and queuing any new items to be
   /// sent to the client.
   pub fn follow_tree(&mut self, kind: DataType, id: u32, listens: &mut Listens) -> Result<()> {
      let mut first = true;
      let predicate = |kind, id| {
         if !first && listens.has(kind, id) {
            return false;
         }
         first = false;
         listens.add(kind, id);
         true
      };

      let mut data = follow_tree(self as &Source, predicate, kind, id)?;
      let mut i = data.len();
      while i > 0 {
         i -= 1;
         let item = data.remove(i);
         let priority = match item {
            DataInner::user(_) => Priority::Low,
            _ => Priority::High,
         };
         self.queue_data(priority, item);
      }

      assert!(data.is_empty());

      Ok(())
   }

   /// Send a notificaiton with high priority and a 2 minute timeout.
   pub fn notify(&mut self, title: String, text: String, icon: NotificationIcon, id: u32) {
      let mut notification = Notification::new();
      notification.set_title(title);
      notification.set_text(text);
      notification.set_icon(icon);
      notification.set_id(id);
      let timeout = Instant::now() + Duration::from_secs(120);
      self.queue_notify(Priority::High, notification, Some(timeout));
   }

   /// Queue a notification to be sent to the client
   pub fn queue_notify(&mut self, priority: Priority, data: Notification, timeout: Option<Instant>) {
      self.out_queue.push(Update::Notification {
         priority: priority,
         timeout: timeout,
         data: data
      });
   }

   /// Queue some data to be sent to the client. The priority doesn't change
   /// the order data is sent, it only effects whether or not it's sent while
   /// the user is inactive.
   pub fn queue_data(&mut self, new_priority: Priority, new_data: DataInner) {

      let (kind, id) = (new_data.kind(), new_data.id());

      if self.update_set.contains(&(kind, id)) {
         // Try to find the data in the queue.
         if let Some(item) = self.out_queue.iter_mut().find(|x|
            if let Update::Data{ref data, ..} = **x { (data.kind(), data.id()) == (kind, id) }
            else { false }) {
            match *item {
               Update::Data{ref mut data, ref mut priority, ..} => {
                  warn!(self.log, "Overwriting data in queue {:?} -> {:?}", data, new_data);
                  // If there's and existing queued update for this data,
                  // update that and return.
                  *priority = cmp::max(*priority, new_priority);
                  *data = new_data.clone();
               }
               // unreachable as the predicate above only returns true for Data.
               _ => unreachable!(),
            }
         } else {
            warn!(self.log, "can't find data referenced by update_set: {:?} {}", kind, id);
         }
      }

      self.update_set.insert((kind, id));

      self.out_queue.push(Update::Data {
         priority: new_priority,
         data: new_data,
         req_id: self.data_add_map.remove(&(kind, id)),
      });
   }

   /// Handle a request to unlock a location or bonus point.
   fn unlock(&mut self, code: Unlock) {
      let id = self.user_id();
      let err = if let Some(ref mut game) = self.game {
         game.unlock(id, code).err()
            .map(|e| format!("Unlock failed: {}", e))
      } else {
         Some("Unlock failed: The hunt hasn't started yet!".to_string())
      };

      if let Some(e) = err { self.err(e); }
   }

   /// Handle a request to add some data.
   fn data_add(&mut self, data: Data) {
      if let Err(e) = self.data_add_inner(data) {
         self.err(format!("Add failed: {}", e));
      }
   }
   fn data_add_inner(&mut self, data: Data) -> Result<()> {
      let item = data.into_inner();
      if item.is_none() {
         bail!("Data item has no data field.");
      }
      let mut item = item.unwrap();

      if item.kind() == DataType::LOGIN_DETAILS {
         if item.is_in(&self.db) {
            bail!("This user already has login details, did you mean to update?");
         }
      } else {
         if item.id() != 0 {
            bail!("Data has non-zero ID ({}), did you mean to update?", item.id());
         }
         item.new_id(&self.db);
      }

      self.check_add(&item)?;

      if let DataInner::login_details(ref mut details) = item {
         let pass = details.get_password().to_string();
         details.set_password(::bcrypt::hash(&pass, 6).unwrap());
      }

      if let DataInner::user(ref mut user) = item {
         if user.has_team_id() {
            let team_id = user.get_team_id();
            if let Some(mut team) = self.db.teams.get_mut(team_id) {
               team.mut_players().push(user.id());
               self.send_ev(GameEv::DataUpdate(DataType::CHAT, team_id));
            }
         }
         if user.has_join_hunt() {
            let hunt_id = user.get_join_hunt();
            if let Some(mut hunt) = self.db.hunts.get_mut(hunt_id) {
               hunt.mut_demonstrators().push(user.id());
               self.send_ev(GameEv::DataUpdate(DataType::HUNT, hunt_id));
            }
         }
      }

      if let DataInner::bonus(ref mut bonus) = item {
         if bonus.has_join_hunt() {
            let hunt_id = bonus.get_join_hunt();
            if let Some(mut hunt) = self.db.hunts.get_mut(hunt_id) {
               hunt.mut_bonuses().push(bonus.id());
               self.send_ev(GameEv::DataUpdate(DataType::HUNT, hunt_id));
            }
         }
      }

      if let DataInner::team(ref mut team) = item {
         if team.has_hunt() {
            let hunt_id = team.get_hunt();
            if let Some(mut hunt) = self.db.hunts.get_mut(hunt_id) {
               hunt.mut_teams().push(team.id());
               self.send_ev(GameEv::DataUpdate(DataType::HUNT, hunt_id));
            }
         }
      }

      if let DataInner::point(ref mut point) = item {
         if point.has_join_hunt() {
            let hunt_id = point.get_join_hunt();
            if let Some(mut hunt) = self.db.hunts.get_mut(hunt_id) {
               hunt.mut_points().push(point.id());
               self.send_ev(GameEv::DataUpdate(DataType::HUNT, hunt_id));
            }
         }
      }

      if let DataInner::chat_message(ref mut msg) = item {
         msg.set_time_sent(UTC::now().timestamp() as u64);
         msg.set_sender_id(self.user_id());

         let group_id = msg.get_chat_id();
         if let Some(mut group) = self.db.chats.get_mut(group_id) {
            {
               let messages = group.mut_messages();
               messages.push(msg.id());
               if messages.len() > MAX_MESSAGE_HISTORY {
                  let id = messages.remove(0);
                  self.db.messages.delete(id);
               }
            }
            self.send_ev(GameEv::DataUpdate(DataType::CHAT, group_id));

            let mut text = msg.get_text().to_string();
            if text.len() > 70 {
               text = format!("{}...", &text[0..67]);
            }
            let mut players = group.get_participants().to_vec();
            if let Some(index) = players.iter().position(|&x| x == self.user_id()) {
               players.swap_remove(index);
            }

            let notify = GameEv::Notify {
               players: players,
               title: format!("Message from {}", self.user.get_name()),
               text: text,
               icon: NotificationIcon::GENERAL,
               id: NotifyID::NewMessage as u32 + group_id,
            };
            self.send_ev(notify);
         }
      }

      self.send_ev(GameEv::DataUpdate(item.kind(), item.id()));
      self.data_add_map.insert((item.kind(), item.id()), self.connection.last_seq());
      self.queue_data(Priority::High, item.clone());

      item.insert_into(&self.db);


      Ok(())
   }

   /// Handle a request to update some data.
   fn data_update(&mut self, data: Data) {
      if let Err(e) = self.data_update_inner(data) {
         self.err(format!("Update failed: {}", e));
      }
   }
   fn data_update_inner(&mut self, data: Data) -> Result<()> {
      let item = data.into_inner();
      if item.is_none() {
         bail!("Data item has no data field.");
      }
      let item = item.unwrap();

      if !item.is_in(&self.db) {
         bail!("{:?}[{}] is not in the database", item.kind(), item.id());
      }

      self.check_update(&item)?;
      self.send_ev(GameEv::DataUpdate(item.kind(), item.id()));
      item.insert_into(&self.db);

      Ok(())
   }

   /// Handle a request to delete some data.
   fn data_delete(&mut self, id: DataID) {
      let (kind, id) = (id.get_field_type(), id.get_ids());
      if let Err(e) = self.data_delete_inner(kind, id) {
         self.err(format!("Delete failed: {}", e));
      }
   }
   fn data_delete_inner(&mut self, kind: DataType, id: u32) -> Result<()> {
      if !self.db.has(kind, id) {
         bail!("{:?}[{}] is not in the database", kind, id)
      }

      self.check_delete(kind, id)?;

      self.send_ev(GameEv::DataDelete(kind, id));
      self.db.delete(kind, id);
      Ok(())
   }

   /// Handle a request to start a treasure hunt.
   fn hunt_start(&mut self, id: u32) {
      if self.permission() == Permission::ADMIN {
         self.send_ev(GameEv::StartHunt(id));
      } else {
         self.err("Only admins can start a game.");
      }
   }

   /// Handle a request to create a new hunt from a JSON definition.
   fn json_hunt(&mut self, json: String) {
      if self.permission() == Permission::ADMIN {
         use std::io::*;

         let mut cursor = Cursor::new(json.into_bytes());
         match serde_json::from_reader::<_, test_data::Hunt>(&mut cursor) {
            Ok(hunt) => {
               let mut users = Vec::new();
               match hunt.to_db(&self.db, &mut users) {
                  Err(e) => self.err(format!("Failed to add hunt: {}", e)),
                  Ok(hunt_id) => {
                     let passwords = users.into_iter().map(|u| {
                        let mut x = LoginDetails::new();
                        x.set_name(u.name);
                        x.set_username(u.username.unwrap());
                        x.set_password(u.password.unwrap());
                        x
                     }).collect();

                     let mut msg_passwords = NewHunt::new();
                     msg_passwords.set_hunt_id(hunt_id);
                     msg_passwords.set_passwords(
                        ::protobuf::RepeatedField::from_vec(passwords));

                     let mut msg = Message::new();
                     msg.set_request_seq(self.connection.last_seq());
                     msg.set_hunt_passwords(msg_passwords);
                     self.connection.send(msg);
                  }
               }
            }
            Err(e) => self.err(format!("Failed to parse JSON: {}", e)),
         }
      } else {
         self.err("Only admins can create hunts.");
      }
   }

   /// Handle receiving a notification from the client? xP
   fn handle_notify(&mut self, notification: Notification) {
      // Send the notification back to the client :3
      let mut reply = Message::new();
      reply.set_notify(notification);
      self.connection.send(reply);
   }

   /// Respond to the previous request with an error message.
   fn err<S: ToString>(&mut self, err: S) {
      let err = err.to_string();
      warn!(self.log, "Request failed: '{}'", err);
      let mut msg = Message::new();
      let mut resp = ReqResult::new();
      resp.set_error(err);
      msg.set_result(resp);
      msg.set_request_seq(self.connection.last_seq());
      self.connection.send(msg);
   }

   fn send_ev(&self, ev: GameEv) {
      if let Err(e) = self.ev_snd.send(ev) {
         warn!(self.log, "Failed to send internal event: {}", e);
      }
   }

   fn handle_ping(&mut self, ping: bool) {
      if ping {
         let mut reply = Message::new();
         reply.set_ping(false);
         self.connection.send(reply);
      } else if let Some(time) = self.ping_sent {
         let diff = Instant::now() - time;
         let ms = diff.as_secs() as f64 * 1000.0 + diff.subsec_nanos() as f64 / 1_000_000.0;
         debug!(self.log, "ping: {:03}ms", ms);
         self.ping_sent = None;
      } else {
         warn!(self.log, "Unexpected ping response");
      }
   }

   /// Check if a message with the given priority should be sent now.
   fn should_send(&self, priority: Priority) -> bool {
      match self.state() {
         PlayerStatus::ACTIVE => true,
         PlayerStatus::ASLEEP => priority == Priority::High,
         PlayerStatus::OFFLINE => false,
      }
   }

   /// Write `self.user` to the database.
   fn write_user(&self) {
      self.user.clone().insert_into(&self.db);
   }

   // ====== Data modification permissions ======= //

   /// Check if the user is allowed to make the given modification to some data.
   fn check_update(&self, item: &DataInner) -> Result<()> {
      if self.permission() == Permission::ADMIN {
         return Ok(());
      }
      match *item {
         DataInner::chat_message(ref msg) => {
            if msg.get_sender_id() != self.user_id() { bail!("You cannot edit another user's messages."); }
         }
         DataInner::user(ref user) => {
            if user.id() != self.user_id() {
               bail!("You cannot edit another user's details.");
            }
            if let Some(old_user) = self.db.users.get(user.id()) {
               if old_user.get_permission() != user.get_permission() {
                  bail!("You cannot change your own permission level");
               }
               if old_user.get_team_id() != user.get_team_id() {
                  bail!("You cannot change your own team");
               }
            } else {
               bail!("The target no longer exists");
            }
         }
         _ => bail!("You don't have permission to edit this value")
      }
      Ok(())
   }

   /// Check if the user is allowed to add some data.
   fn check_add(&self, item: &DataInner) -> Result<()> {
      if self.permission() == Permission::ADMIN {
         return Ok(());
      }
      match *item {
         DataInner::chat_message(ref msg) => {
            self.check_send_chat(msg.get_chat_id())
         }
         _ => bail!("You don't have permission to add this data"),
      }
   }

   /// Check if the user is allowed to delete some data.
   fn check_delete(&self, _kind: DataType, _id: u32) -> Result<()> {
      if self.permission() != Permission::ADMIN {
         bail!("You don't have permission to delete this data");
      }
      Ok(())
   }

   /// Check if the user has permission to read messages from a chat group.
   fn check_read_chat(&self, group_id: u32) -> Result<()> {
      if self.permission() == Permission::ADMIN {
         return Ok(());
      }

      if let Some(group) = self.db.chats.get(group_id) {
         let can_send = group.get_participants().contains(&self.user_id());

         if can_send || group.get_is_broadcast() {
            Ok(())
         } else {
            bail!("Chat group read permission denied")
         }
      } else {
         bail!("Invalid chat group ID")
      }
   }

   /// Check if the user has permission to send messages to a chat group.
   fn check_send_chat(&self, group_id: u32) -> Result<()> {
      if let Some(group) = self.db.chats.get(group_id) {
         if !group.get_participants().contains(&self.user_id()) {
            bail!("Chat group send permission denied");
         }
         Ok(())
      } else {
         bail!("Invalid chat group ID")
      }
   }
}

impl Source for Client {
   /// Get the user with the given ID from the perspective of this user.
   fn get_user(&self, id: u32) -> Result<User> {
      let mut user = if let Some(user) = self.db.users.get(id) {
         user.clone()
      } else { bail!("User doesn't exist") };

      if id == self.user.get_id() { return Ok(user) }

      match self.permission() {
         Permission::ADMIN | Permission::DEMONSTRATOR => {
            Ok(user)
         }
         Permission::PLAYER => {
            if self.user.has_team_id() && user.has_team_id() &&
               self.user.get_team_id() == user.get_team_id() {
               Ok(user)
            } else if user.get_permission() == Permission::DEMONSTRATOR {
               user.clear_location();
               user.clear_status();
               user.clear_team_id();
               Ok(user)
            } else {
               bail!("Can't get data for a player in another team.")
            }
         }
      }
   }

   /// Get the team with the given ID from the perspective of this user.
   fn get_team(&self, id: u32) -> Result<Team> {
      let mut team = if let Some(team) = self.db.teams.get(id) {
         team.clone()
      } else { bail!("Team doesn't exist") };

      let in_team = self.user.has_team_id() && self.user.get_team_id() == id;

      if self.permission() == Permission::PLAYER && !in_team {
         // If the player doesn't belong to this team, ommit the player,
         // point and bonus lists.
         team.set_points(RepeatedField::new());
         team.clear_players();
         team.clear_bonuses();
      }
      Ok(team)
   }

   /// Get the hunt with the given ID from the perspective of this user.
   fn get_hunt(&self, id: u32) -> Result<Hunt> {
      let mut hunt = if let Some(hunt) = self.db.hunts.get(id) {
         hunt.clone()
      } else { bail!("Hunt doesn't exist") };

      if self.permission() == Permission::PLAYER {
         hunt.clear_points();

         // Include only the chat groups for which this player is a participant.
         let chats = hunt.take_chats().into_iter().filter(|id| {
            self.check_read_chat(*id).is_ok()
         }).collect();
         hunt.set_chats(chats);
      }
      Ok(hunt)
   }

   /// Get the point with the given ID from the perspective of this user.
   fn get_point(&self, id: u32) -> Result<Point> {
      let mut point = if let Some(point) = self.db.points.get(id) {
         point.clone()
      } else { bail!("Point doesn't exist") };

      if self.permission() == Permission::PLAYER {
         point.clear_password();

         // Try to find the point in the user's team's list, if the point isn't
         // unlocked yet, clear it's location.
         self.db.teams.get(self.user.get_team_id())
            .and_then(|team| team.get_points().iter().find(|p| p.get_id() == id).cloned())
            .map(|p|
               if !p.has_time_found() {
                  point.clear_location();
               })
            .ok_or("Point not unlocked".into(): Error)?;
      }

      Ok(point)
   }

   /// Get the bonus with the given ID from the perspective of this user.
   fn get_bonus(&self, id: u32) -> Result<Bonus> {
      let mut bonus = if let Some(bonus) = self.db.bonuses.get(id) {
         bonus.clone()
      } else { bail!("Bonus doesn't exist") };

      if self.permission() == Permission::PLAYER {
         // Try to find the bonus in the user's team's list, if the point isn't
         // unlocked yet, clear it's location.
         self.db.teams.get(self.user.get_team_id())
            .map(|team| team.get_bonuses().contains(&id))
            .map(|unlocked| if !unlocked {
               bonus.clear_location();
               bonus.clear_name();
            })
            .ok_or("User's team not found".into(): Error)?;
      }

      Ok(bonus)
   }

   /// Get the chat group with the given ID from the perspective of this user.
   fn get_chat(&self, id: u32) -> Result<Chat> {
      self.check_read_chat(id)?;

      if let Some(chat) = self.db.chats.get(id) {
         Ok(chat.clone())
      } else {
         bail!("Chat group doesn't exist")
      }
   }

   /// Get the chat message with the given ID from the perspective of this user.
   fn get_message(&self, id: u32) -> Result<ChatMessage> {
      let message = if let Some(message) = self.db.messages.get(id) {
         message.clone()
      } else { bail!("Message doesn't exist") };

      self.check_read_chat(message.get_chat_id())?;
      Ok(message)
   }

   /// Attempt to get the login details for the user with the given ID.
   fn get_login_details(&self, id: u32) -> Result<LoginDetails> {
      if self.permission() == Permission::ADMIN {
         if let Some(data) = self.db.login.get(id) {
            Ok(data.clone())
         } else {
            bail!("User doesn't exist")
         }
      } else {
         bail!("Only admins can access login details.")
      }
   }
}
