//!
//! A high-level connection to a client. Buffers messages for re-sending
//! on session resumption after loosing connection.

use message::*;
use buffer::*;
use socket::ClientEv;
use futures::sync::mpsc::*;
use std::cmp;
use std::cell::Cell;
use std::time::*;

/// A connecion to a single client.
pub struct Connection {
   log: ::Log,
   /// The connection ID.
   id: u64,
   /// A sender for outgoing messages: (clientID, message).
   snd: UnboundedSender<ClientEv>,
   /// The last message sent for which we have received an acknowledement.
   last_ack: u32,
   /// The sequence number for outgoing messages.
   out_seq: u32,
   /// The sequence number of the last message we received.
   in_seq: u32,
   /// A buffer of messages we have sent, so we can resend data if the connection
   /// is broken.
   buffer: Buffer<OldMessage>,
   /// True if we haven't received acknowledgements for old messages.
   buffer_full: bool,
   /// The time we last received a message
   last_rec_time: Instant,
   shutdown: Cell<bool>,
}

struct OldMessage {
   message: Message,
   /// The time at which this message is no longer relevant, used to prevent
   /// sending old notifications e.g. for time limited events.
   timeout: Option<Instant>,
}

/// The size of the outgoing message buffer.
const BUFFER_SIZE: u64 = 1024;

/// The time in milliseconds to wait for an ACK before closing the connection.
const ACK_WAIT_TIME: u64 = 30_000;

impl Connection {
   /// Create a new `Connection`.
   pub fn new(log: ::Log, id: u64, snd: UnboundedSender<ClientEv>) -> Connection {
      Connection {
         log,
         id,
         snd,
         last_ack: 0,
         out_seq: 0,
         in_seq: 0,
         buffer: Buffer::with_capacity(BUFFER_SIZE),
         buffer_full: false,
         last_rec_time: Instant::now(),
         shutdown: Cell::new(false),
      }
   }

   /// Returns the connection's ID.
   pub fn id(&self) -> u64 { self.id }

   /// Shutdown the connection if our buffer is full or we're waiting on an ACK
   /// and haven't received a message in 10 seconds.
   pub fn clean(&self, now: Instant) {
      if self.shutdown.get() { return; }
      if self.out_seq == 0 { return; }
      if self.buffer_full ||
         (self.out_seq - 1 > self.last_ack &&
         (now - self.last_rec_time) > Duration::from_millis(ACK_WAIT_TIME)) {

         if self.buffer_full {
            info!(self.log, "Connection lost, buffer full");
         } else {
            info!(self.log, "Connection lost, {}ms without ACK", ACK_WAIT_TIME);
         }

         self.shutdown();
      }
   }

   /// Shutdown the connection.
   pub fn shutdown(&self) {
      if !self.shutdown.get() {
         info!(self.log, "shutdown");
         let _ = self.snd.send(ClientEv::Shutdown);
         self.shutdown.set(true);
      }
   }

   /// Get the sequence number of the last message we received.
   pub fn last_seq(&self) -> u32 { self.in_seq }

   /// Handle an incoming message from the client.
   pub fn handle(&mut self, msg: Message) -> Option<MessageData> {
      self.last_rec_time = Instant::now();

      if msg.get_transport().is_none() {
         warn!(self.log, "Got a message with no seq or ack");
         return None;
      }

      let mut is_ack = false;

      match msg.get_transport().clone().unwrap() {
         MessageTransport::seq(seq) => self.in_seq = seq,
         MessageTransport::ack(ack) => {
            self.last_ack = cmp::max(self.last_ack, ack);
            is_ack = true;
         }
      };

      if !msg.get_msg_data().is_some() {
         if !is_ack {
            warn!(self.log, "Got a message with missing or unknown opcode");
         }
         return None;
      }

      msg.take_msg_data()
   }

   /// Send a message to the client.
   pub fn send(&mut self, msg: Message) {
      self.send_with_timeout(msg, None);
   }

   /// Send a message with an optional timeout -- when resuming a broken
   /// connection messages which have timed-out are not re-sent.
   pub fn send_with_timeout(&mut self, mut msg: Message, timeout: Option<Instant>) {
      if self.shutdown.get() { return; }
      if self.out_seq - self.last_ack > (BUFFER_SIZE - 2) as u32 {
         self.buffer_full = true;
         return;
      }

      msg.set_seq(self.out_seq);
      self.buffer.add(OldMessage{ message: msg.clone(), timeout: timeout });
      self.out_seq += 1;

      if let Err(e) = self.snd.send(ClientEv::Message(msg)) {
         error!(self.log, "Failed to send: {} ", e);
         self.shutdown();
      }
   }

   /// Attempt to resume a connection, resending any messages the client
   /// is missing. Returns true after resending, or false if we no longer have
   /// the data we need in our buffer.
   pub fn resume(&self, new: &mut Connection) -> bool {
      let now = Instant::now();
      for i in (self.last_ack + 1)..self.out_seq {
         if let Some(msg) = self.buffer.get(i as u64) {
            // Check the messages timeout, and don't send if it's expired.
            let send = msg.timeout.map_or(true, |time| time > now);
            if send {
               new.send(msg.message.clone());
            }
         } else {
            return false;
         }
      }
      true
   }
}
