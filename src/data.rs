use message::*;
use protobuf::*;
use error::*;
use database::Database;

/// Some data type
pub trait DataTrait: Sized {
   /// Get the ID of the item.
   fn id(&self) -> u32;
   /// Get the kind of the item.
   fn kind(&self) -> DataType;
   /// Check if the database has an item with the same ID as this one.
   fn is_in(&self, db: &Database) -> bool;
   /// Add the item to the database.
   fn insert_into(self, db: &Database);
   /// Assign a new unique ID to this object.
   fn new_id(&mut self, db: &Database);
   /// Check the validity of this item's references in the given database
   fn integrity_check(&self, db: &Database) -> Result<()>;
}

/// Some data which can be stored in a database.
/// Implemented for all protobuf messages which implement `Data`.
pub trait DbData: DataTrait + MessageStatic + Sized { }
impl<T: DataTrait + MessageStatic + Sized> DbData for T { }

/// A node in a tree of data.
pub trait Node {
   /// Get the IDs of any chidlren.
   fn children(&self) -> Vec<(DataType, Vec<u32>)>;
   /// Get the ID of the parent, or `None` if there is no parent.
   fn parent(&self) -> Option<(DataType, u32)>;
}

/// A source of data. We abstract over these functions to allow `Client` to filter
/// the data available to itself when using `follow_tree`, e.g. if the client is
/// a player, we don't want to give them the player lists for other teams.
pub trait Source {
   /// Get the point with the given ID from the view of this source.
   fn get_point(&self, id: u32) -> Result<Point>;

   /// Get the team with the given ID from the view of this source.
   fn get_team(&self, id: u32) -> Result<Team>;

   /// Get the hunt with the given ID from the view of this source.
   fn get_hunt(&self, id: u32) -> Result<Hunt>;

   /// Get the user with the given ID from the view of this source.
   fn get_user(&self, id: u32) -> Result<User>;

   /// Get the bonus point with the given ID from the view of this source.
   fn get_bonus(&self, id: u32) -> Result<Bonus>;

   /// Get the chat group with the given ID from the view of this source.
   fn get_chat(&self, id: u32) -> Result<Chat>;

   /// Get the chat message with the given ID from the view of this source.
   fn get_message(&self, id: u32) -> Result<ChatMessage>;

   /// Get the login details for the given user ID from the view of this source.
   fn get_login_details(&self, id: u32) -> Result<LoginDetails>;
}

// A macro which implements `Data` for some type. See the examples below for
// usage. The given type must:
//  - implement `Node` for integrity checking;
//  - have an associated collection in `Database`;
//  - have functions to get and set some unique ID (used as a key for the database);
//  - have an associated DataType value.

macro_rules! impl_data_trait {
   ($t:ident, $kind:ident, $db_name:ident, $get_id:ident, $set_id:ident) => {
      impl DataTrait for $t {
         fn id(&self) -> u32 {
            self.$get_id()
         }
         fn kind(&self) -> DataType {
            DataType::$kind
         }
         fn is_in(&self, db: &Database) -> bool {
            db.$db_name.has(self.id())
         }
         fn insert_into(self, db: &Database) {
            db.$db_name.insert(self);
         }
         fn new_id(&mut self, db: &Database) {
            self.$set_id(db.$db_name.next_id())
         }
         fn integrity_check(&self, db: &Database) -> Result<()> {
            if let Some((kind, id)) = self.parent() {
               if !db.has(kind, id) {
                  bail!("({:?}) is missing parent {:?} {}", self, kind, id)
               }
            }
            for (kind, ids) in self.children() {
               for id in ids {
                  if !db.has(kind, id) {
                     bail!("({:?}) is missing child {:?} {}", self, kind, id)
                  }
               }
            }
            Ok(())
         }
      }
   }
}

impl_data_trait!(Hunt, HUNT, hunts, get_id, set_id);
impl_data_trait!(User, USER, users, get_id, set_id);
impl_data_trait!(Team, TEAM, teams, get_id, set_id);
impl_data_trait!(Point, POINT, points, get_id, set_id);
impl_data_trait!(Bonus, BONUS, bonuses, get_id, set_id);
impl_data_trait!(Chat, CHAT, chats, get_id, set_id);
impl_data_trait!(ChatMessage, CHAT_MESSAGE, messages, get_id, set_id);
impl_data_trait!(LoginDetails, LOGIN_DETAILS, login, get_user_id, set_user_id);

// A macro to match over a `DataInner` value and call the given function for
// each possible value, used to implement `DataTrait` for `DataInner` by
// delegating functions to the inner value.
macro_rules! match_inner {
   ($self:expr, $x:pat => $fn:expr) => {
      match $self {
          DataInner::hunt($x) => $fn,
          DataInner::user($x) => $fn,
          DataInner::team($x) => $fn,
          DataInner::point($x) => $fn,
          DataInner::bonus($x) => $fn,
          DataInner::chat($x) => $fn,
          DataInner::chat_message($x) => $fn,
          DataInner::login_details($x) => $fn,
      }
   }
}

impl DataTrait for DataInner {
   fn id(&self) -> u32 {
      match_inner!(*self, ref x => x.id())
   }
   fn kind(&self) -> DataType {
      match_inner!(*self, ref x => x.kind())
   }
   fn is_in(&self, db: &Database) -> bool {
      match_inner!(*self, ref x => x.is_in(db))
   }
   fn insert_into(self, db: &Database) {
      match_inner!(self, x => x.insert_into(db))
   }
   fn new_id(&mut self, db: &Database) {
      match_inner!(*self, ref mut x => x.new_id(db))
   }
   fn integrity_check(&self, db: &Database) -> Result<()> {
      match_inner!(*self, ref x => x.integrity_check(db))
   }
}

/// Follow the data tree of the given item, testing the predicate for each item,
/// and returning a collection of all the associated data in an order which
/// ensures referential integrity.
pub fn follow_tree<P>(source: &Source,
                  mut predicate: P,
                  kind: DataType,
                  id: u32) -> Result<Vec<DataInner>>
                  where P: FnMut(DataType, u32)
                  -> bool
{
   let mut data = Vec::new();
   follow_branch(source, &mut predicate, kind, id, 0, &mut data)?;
   Ok(data)
}

// A macro which branches out from some value in a data tree, calling $branch
// for it's parent and $for_all for it's set of children. Used to implement
// follow_branch.
macro_rules! branch_on_type {
   ($f:expr, $t:ident, $branch:ident, $for_all:ident) => {{
      let x = $f?;
      x.parent().map_or(Ok(()), &mut $branch)?;
      $for_all(x.children(), &mut $branch)?;
      Ok(DataInner::$t(x))
   }}
}

/// The recursive implementation of `follow_tree`. The maximum recursion depth
/// is hard-coded to 100 recursive calls, if this is exceeded an error is returned.
fn follow_branch<P>(source: &Source,
                    mut predicate: &mut P,
                    kind: DataType,
                    id: u32,
                    depth: u32,
                    data: &mut Vec<DataInner>) -> Result<()>
                    where P: FnMut(DataType, u32)
                    -> bool
{
   if depth > 100 { bail!("recursed too deeply"); }
   if !predicate(kind, id) { return Ok(()); }

   let result: Result<DataInner> = {
      // Branch to the given data
      let mut branch = |(kind, id)| {
         follow_branch(source, predicate, kind, id, depth + 1, data)
      };

      // Branch for each item in the given set
      let for_all = |ids, branch: &mut FnMut(_) -> Result<()>| -> Result<()> {
         for (kind, ids) in ids {
            for id in ids { branch((kind, id))? }
         }
         Ok(())
      };

      // Match on the data type -- get the relevant data and branch on it's
      // parent & children.
      match kind {
         DataType::POINT => branch_on_type!(source.get_point(id), point, branch, for_all),
         DataType::TEAM => branch_on_type!(source.get_team(id), team, branch, for_all),
         DataType::HUNT => branch_on_type!(source.get_hunt(id), hunt, branch, for_all),
         DataType::USER => branch_on_type!(source.get_user(id), user, branch, for_all),
         DataType::BONUS => branch_on_type!(source.get_bonus(id), bonus, branch, for_all),
         DataType::CHAT => branch_on_type!(source.get_chat(id), chat, branch, for_all),
         DataType::CHAT_MESSAGE => branch_on_type!(source.get_message(id), chat_message, branch, for_all),
         DataType::LOGIN_DETAILS => branch_on_type!(source.get_login_details(id), login_details, branch, for_all),
      }
   };

   // We add the item after recursing, such that all it's dependencies are
   // satisfied before we send this data, giving us referential integrity.
   data.push(result?);
   Ok(())
}

// Here we define the structure of the tree.
// For each type, we implement functions to get the ID and type of the value's
// parent and any children. Note that since this uses specific instances of the
// type, values which have been filtered could show a subset of the actual
// value's children. This allows us to e.g. ommit the members of a team some
// user doesn't belong to, or only send the subset of _unlocked_ points in a hunt.

impl Node for Team {
   fn children(&self) -> Vec<(DataType, Vec<u32>)> {
      let points = self.get_points().iter().map(|p| p.get_id()).collect();
      let players = self.get_players().to_vec();

      vec![(DataType::POINT, points), (DataType::USER, players)]
   }
   fn parent(&self) -> Option<(DataType, u32)> {
      Some((DataType::HUNT, self.get_hunt()))
   }
}

impl Node for Hunt {
   fn parent(&self) -> Option<(DataType, u32)> { None }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> {
      vec![
         (DataType::TEAM, self.get_teams().to_vec()),
         (DataType::POINT, self.get_points().to_vec()),
         (DataType::BONUS, self.get_bonuses().to_vec()),
         (DataType::CHAT, self.get_chats().to_vec()),
      ]
   }
}

impl Node for User {
   fn parent(&self) -> Option<(DataType, u32)> {
      if self.has_team_id() {
         Some((DataType::TEAM, self.get_team_id()))
      } else { None }
   }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> { Vec::new() }
}

impl Node for Point {
   fn parent(&self) -> Option<(DataType, u32)> { None }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> { Vec::new() }
}

impl Node for Bonus {
   fn parent(&self) -> Option<(DataType, u32)> { None }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> { Vec::new() }
}

impl Node for Chat {
   fn parent(&self) -> Option<(DataType, u32)> { None }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> {
      vec![
         (DataType::USER, self.get_participants().to_vec()),
         (DataType::CHAT_MESSAGE, self.get_messages().to_vec())
      ]
   }
}

impl Node for ChatMessage {
   fn parent(&self) -> Option<(DataType, u32)> {
      Some((DataType::CHAT, self.get_chat_id()))
   }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> { Vec::new() }
}

impl Node for LoginDetails {
   fn parent(&self) -> Option<(DataType, u32)> { None }
   fn children(&self) -> Vec<(DataType, Vec<u32>)> { Vec::new() }
}
