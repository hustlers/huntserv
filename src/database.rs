use protobuf::*;
use byteorder::*;
use fnv::FnvHashMap as HashMap;
use antidote::RwLock as ARwLock;
use std::sync::*;
use std::sync::atomic::*;
use std::path::*;
use std::fs;
use std::io;

use data::*;
use error::*;
use message;

pub use guardian::ArcRwLockReadGuardian as ReadGuard;
pub use guardian::ArcRwLockWriteGuardian as WriteGuard;

/// A simple file-backed database. Data will be saved when the `Database`
/// is dropped.
#[derive(Debug)]
pub struct Database {
   root: PathBuf,
   /// A collection of users
   pub users: Collection<message::User>,
   /// A collection of teams
   pub teams: Collection<message::Team>,
   /// A collection of main points
   pub points: Collection<message::Point>,
   /// A collection of treasure hunts
   pub hunts: Collection<message::Hunt>,
   /// A collection of bonus points
   pub bonuses: Collection<message::Bonus>,
   /// A collection of chat groups
   pub chats: Collection<message::Chat>,
   /// A collection of chat messages
   pub messages: Collection<message::ChatMessage>,
   /// A collection of login details
   pub login: Collection<message::LoginDetails>,
}

/// A collection of shared data
#[derive(Debug)]
pub struct Collection<T: ::std::fmt::Debug> {
   log: ::Log,
   dirty: AtomicBool,
   key: AtomicU32,
   data: ARwLock<HashMap<u32, Item<T>>>,
}

/// A single shared item
#[derive(Debug)]
struct Item<T> {
   data: Arc<RwLock<T>>,
}

impl Database {
   /// Create a new `Database`.
   pub fn new<P: AsRef<Path>>(root: P, log: ::Log) -> Database {
      Database {
         root: root.as_ref().to_path_buf(),
         users: Collection::new(log.new(o!("db" => "users"))),
         teams: Collection::new(log.new(o!("db" => "teams"))),
         points: Collection::new(log.new(o!("db" => "points"))),
         hunts: Collection::new(log.new(o!("db" => "hunts"))),
         bonuses: Collection::new(log.new(o!("db" => "bonuses"))),
         chats: Collection::new(log.new(o!("db" => "chats"))),
         messages: Collection::new(log.new(o!("db" => "messages"))),
         login: Collection::new(log.new(o!("db" => "login"))),
      }
   }

   /// Load a `Database` from a directory.
   pub fn load<P: AsRef<Path>>(root: P, log: ::Log) -> Result<Database> {
      let root = root.as_ref();
      info!(log, "Loading database from {:?}", root);
      let db = Database {
         root: root.to_path_buf(),
         users: Collection::from_file(root.join("users.db"), log.new(o!("db" => "users")))?,
         teams: Collection::from_file(root.join("teams.db"), log.new(o!("db" => "teams")))?,
         points: Collection::from_file(root.join("points.db"), log.new(o!("db" => "points")))?,
         hunts: Collection::from_file(root.join("hunts.db"), log.new(o!("db" => "hunts")))?,
         bonuses: Collection::from_file(root.join("bonuses.db"), log.new(o!("db" => "bonuses")))?,
         chats: Collection::from_file(root.join("chats.db"), log.new(o!("db" => "chats")))?,
         messages: Collection::from_file(root.join("messages.db"), log.new(o!("db" => "messages")))?,
         login: Collection::from_file(root.join("login.db"), log.new(o!("db" => "login")))?,
      };
      db.integrity_check()?;
      Ok(db)
   }

   /// Save the `Database`
   pub fn save(&self) -> Result<()> {
      self.save_to(&self.root)
   }

   /// Save any collections which have been modified
   pub fn save_changed(&self) -> Result<()> {
      self.save_changed_to(&self.root)
   }

   /// Save the `Database` to the given directory
   pub fn save_to<P: AsRef<Path>>(&self, root: P) -> Result<()> {
      let root = root.as_ref();
      self.users.save(root.join("users.db"))?;
      self.teams.save(root.join("teams.db"))?;
      self.points.save(root.join("points.db"))?;
      self.hunts.save(root.join("hunts.db"))?;
      self.bonuses.save(root.join("bonuses.db"))?;
      self.chats.save(root.join("chats.db"))?;
      self.messages.save(root.join("messages.db"))?;
      self.login.save(root.join("login.db"))?;
      Ok(())
   }

   /// Save any collections which have been modified to the given directory
   pub fn save_changed_to<P: AsRef<Path>>(&self, root: P) -> Result<()> {
      let root = root.as_ref();
      self.users.save_changed(root.join("users.db"))?;
      self.teams.save_changed(root.join("teams.db"))?;
      self.points.save_changed(root.join("points.db"))?;
      self.hunts.save_changed(root.join("hunts.db"))?;
      self.bonuses.save_changed(root.join("bonuses.db"))?;
      self.chats.save_changed(root.join("chats.db"))?;
      self.messages.save_changed(root.join("messages.db"))?;
      self.login.save_changed(root.join("login.db"))?;
      Ok(())
   }

   /// Test if the database contains some data with the given type and ID.
   pub fn has(&self, kind: message::DataType, id: u32) -> bool {
      use message::DataType;
      match kind {
         DataType::USER => self.users.has(id),
         DataType::TEAM => self.teams.has(id),
         DataType::POINT => self.points.has(id),
         DataType::HUNT => self.hunts.has(id),
         DataType::BONUS => self.bonuses.has(id),
         DataType::CHAT => self.chats.has(id),
         DataType::CHAT_MESSAGE => self.messages.has(id),
         DataType::LOGIN_DETAILS => self.login.has(id),
      }
   }

   /// Delete the data with the given type and ID.
   pub fn delete(&self, kind: message::DataType, id: u32) {
      use message::DataType;
      match kind {
         DataType::USER => {self.users.delete(id);}
         DataType::TEAM => {self.teams.delete(id);}
         DataType::POINT => {self.points.delete(id);}
         DataType::HUNT => {self.hunts.delete(id);}
         DataType::BONUS => {self.bonuses.delete(id);}
         DataType::CHAT => {self.chats.delete(id);}
         DataType::CHAT_MESSAGE => {self.messages.delete(id);}
         DataType::LOGIN_DETAILS => {self.login.delete(id);}
      }
   }

   /// Check the referential integrity of the database -- that all referenced
   /// values actually exist. Fails with an error if any references are missing.
   pub fn integrity_check(&self) -> Result<()> {
      self.users.integrity_check(self)?;
      self.teams.integrity_check(self)?;
      self.points.integrity_check(self)?;
      self.hunts.integrity_check(self)?;
      self.bonuses.integrity_check(self)?;
      self.chats.integrity_check(self)?;
      self.messages.integrity_check(self)?;
      self.login.integrity_check(self)?;
      Ok(())
   }

   /// Check if there is a user with the given username
   pub fn has_username(&self, username: &str) -> bool {
      for user_id in self.login.id_set() {
         if let Some(user) = self.login.get(user_id) {
            if user.get_username() == username { return true; }
         }
      }
      false
   }
}

impl<T: DbData + ::std::fmt::Debug> Collection<T> {
   /// Create a new `Collection`
   pub fn new(log: ::Log) -> Collection<T> {
      Collection {
         log: log,
         dirty: AtomicBool::new(false),
         key: AtomicU32::new(1),
         data: ARwLock::new(HashMap::default()),
      }
   }

   /// Check that all the values pointed to by values in this collection actually
   /// exist.
   pub fn integrity_check(&self, db: &Database) -> Result<()> {
      for x in self.data.read().values() {
         x.read().integrity_check(db)?;
      }
      Ok(())
   }

   /// Load a collection from a file, creating it if none exists.
   pub fn from_file<P: AsRef<Path>>(path: P, log: ::Log) -> Result<Collection<T>> {
      let path = path.as_ref();
      match fs::File::open(path) {
         Ok(ref mut file) => {
            Collection::read_from(file, log)
         }
         Err(e) => {
            if e.kind() == io::ErrorKind::NotFound {
               // If the file doesn't exist, create a new collection.
               let collection = Collection::new(log);
               collection.set_dirty();
               collection.save(path)?;
               Ok(collection)
            } else {
               Err(e.into())
            }
         }
      }
   }

   /// Read a collection from a data stream
   pub fn read_from<R: io::Read>(stream: &mut R, log: ::Log) -> Result<Collection<T>> {
      let key = stream.read_u32::<BigEndian>()?;
      let len = stream.read_u32::<BigEndian>()? as usize;
      let mut buffer = Vec::with_capacity(512);

      let collection = Collection::new(log);
      collection.key.store(key, Ordering::Relaxed);
      debug!(collection.log, "Loading {} entries", len; "key" => key);

      {
         let mut data = collection.data.write();
         while data.len() < len {
            let item_len = stream.read_u32::<BigEndian>()? as usize;
            buffer.resize(item_len, 0u8);
            stream.read_exact(&mut buffer)?;

            let item = parse_from_bytes::<T>(&buffer)?;
            let id = item.id();
            let item = Item::new(item);
            data.insert(id, item);
         }
      }

      Ok(collection)
   }

   /// Save the `Collection` iff there have been any changes since the last save
   pub fn save_changed<P: AsRef<Path>>(&self, path: P) -> Result<()> {
      if self.dirty.compare_and_swap(true, false, Ordering::Relaxed) {
         self.save(path)
      } else {
         Ok(())
      }
   }

   /// Set the `dirty` flag, indicating that the database should be saved.
   pub fn set_dirty(&self) {
      self.dirty.store(true, Ordering::Relaxed);
   }

   /// Save the collection to a file.
   pub fn save<P: AsRef<Path>>(&self, path: P) -> Result<()> {
      let mut file = fs::OpenOptions::new()
         .write(true)
         .truncate(true)
         .create(true)
         .open(path)?;

      self.write_to(&mut file)
   }

   /// Write the collection to a stream
   pub fn write_to<R: io::Write>(&self, stream: &mut R) -> Result<()> {
      let data = self.data.read();
      let key = self.key.load(Ordering::Relaxed);
      let mut buffer = Vec::with_capacity(512);

      debug!(self.log, "Saving {} entries", data.len(); "key" => key);
      stream.write_u32::<BigEndian>(key)?;
      stream.write_u32::<BigEndian>(data.len() as u32)?;

      for item in data.values() {
         buffer.clear();
         item.read().write_to_vec(&mut buffer)?;
         stream.write_u32::<BigEndian>(buffer.len() as u32)?;
         stream.write_all(&buffer)?;
      }

      stream.flush()?;
      Ok(())
   }

   /// Test if the collection contains the given ID
   pub fn has(&self, id: u32) -> bool {
      self.data.read().contains_key(&id)
   }

   /// Get an item from the collection.
   pub fn get(&self, id: u32) -> Option<ReadGuard<T>> {
      self.data.read().get(&id).map(|item| item.read())
   }

   /// Get a mutable reference to an item in the collection.
   pub fn get_mut(&self, id: u32) -> Option<WriteGuard<T>> {
      self.set_dirty();
      self.data.read().get(&id).map(|item| item.write())
   }

   /// Get the next ID to use for a new item
   pub fn next_id(&self) -> u32{
      self.set_dirty();
      self.key.fetch_add(1, Ordering::Relaxed)
   }

   /// Find the first item for which the given predicate is true.
   pub fn find<P: Fn(&T) -> bool>(&self, predicate: P) -> Option<ReadGuard<T>> {
      let data = self.data.read();
      for item in data.values() {
         let matches = predicate(&*(item.data.read().unwrap_or_else(|e| e.into_inner())));
         if matches { return Some(item.read()); }
      }
      None
   }

   /// Insert an item.
   pub fn insert(&self, item: T) {
      if !item.is_initialized() {
         error!(self.log, "Item not initialized: {:?}", item);
         return;
      }
      self.set_dirty();
      let id = item.id();
      let item = Item::new(item);
      let _ = self.data.write().insert(id, item);
   }

   /// Delete an item, returning the item if it exists.
   pub fn delete(&self, id: u32) -> Option<Arc<RwLock<T>>> {
      self.set_dirty();
      self.data.write().remove(&id).map(|x| x.into_inner())
   }

   /// Get the set of used IDs.
   pub fn id_set(&self) -> Vec<u32> {
      self.data.read().keys().cloned().collect()
   }
}

impl<T> Item<T> {
   /// Create a new `Item`.
   fn new(data: T) -> Item<T> {
      Item { data: Arc::new(RwLock::new(data)) }
   }

   /// Take a `ReadGuard` for the data.
   fn read(&self) -> ReadGuard<T> {
      ReadGuard::take(self.data.clone())
         .unwrap_or_else(|e| e.into_inner())
   }

   /// Take a `WriteGuard` for the data.
   fn write(&self) -> WriteGuard<T> {
      WriteGuard::take(self.data.clone())
         .unwrap_or_else(|e| e.into_inner())
   }

   /// Take the inner value, consuiming `self`.
   fn into_inner(self) -> Arc<RwLock<T>> {
      self.data
   }
}
