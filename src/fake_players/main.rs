#[macro_use] extern crate slog;
extern crate huntserv;
extern crate tokio_signal;
extern crate tokio_core;
extern crate tokio_timer;
extern crate futures;
extern crate serde;
extern crate serde_json;

use std::fs::File;
use std::collections::HashSet;
use std::time::*;
use tokio_core::reactor::{Core, Handle};
use tokio_timer::*;
use futures::*;

use huntserv::test_data;
use huntserv::socket::*;
use huntserv::message::*;

fn main() {
   let log = huntserv::logger();
   let file = File::open("data.json").unwrap();
   let hunt: test_data::Hunt = serde_json::from_reader(file).unwrap();

   let mut players = Vec::new();

   let mut usernames = HashSet::new();
   for team in hunt.teams {
      for player in team.players {
         let username = test_data::make_username(&player, None).unwrap();
         usernames.insert(username.clone());
         let password = "password".to_string();
         players.push((username, password));
      }
   }

   // Create a 'reactor core', an event loop on which futures run.
   let mut core = Core::new().unwrap();
   let handle = core.handle();
   let timer = Timer::default();

   let c_handle = handle.clone();
   let c_log = log.clone();
   let new_clients = timer.interval(Duration::from_millis(300))
      .for_each(move |()| {
         if let Some((user, pw)) = players.pop() {
            spawn_client(&c_log, &c_handle, user, pw)
         }
         Ok(())
      }).map_err(|e| panic!("{}", e));

   core.run(new_clients).unwrap();
}

fn login(username: &str, password: &str) -> Message {
   let mut message = Message::new();
   let mut login = Login::new();
   login.set_username(username.to_string());
   login.set_password(password.to_string());
   message.set_login_request(login);
   message
}

fn ack(message: &Message) -> Message {
   let mut ack = Message::new();
   ack.set_ack(message.get_seq());
   ack
}


fn spawn_client(log: &huntserv::Log, handle: &Handle, username: String, password: String) {
   let conf = ClientConfig {
      log: log.clone(),
      server_addr: "163.172.153.28:25255".parse().unwrap(),
      server_domain: "hunt.xa1.uk".to_string(),
   };

   println!("Spawning a client");
   let handshake = connect(conf, handle);
   let _handle = handle.clone();

   let done = handshake
      .map_err(|e| panic!("{}", e))
      .and_then(move |stream| {
         let (mut sink, stream) = stream.split();
         let mut login = login(&username, &password);
         login.set_seq(0);

         println!("Starting a client");
         let _ = sink.start_send(login);
         let _ = sink.poll_complete();

         let incoming = stream
            .map_err(|e| panic!("stream error {}", e))
            .for_each(move |msg| {
               println!("Got a message: {:?}", msg);
               if msg.has_seq() {
                  let _ = sink.start_send(ack(&msg));
                  let _ = sink.poll_complete();
               }
               if msg.has_ping() && msg.get_ping() {
                  let mut pong = Message::new();
                  pong.set_seq(0);
                  pong.set_ping(false);
                  let _ = sink.start_send(pong);
                  let _ = sink.poll_complete();
               }
               Ok(())
            });
         _handle.spawn(incoming);
         Ok(())
      });

   handle.spawn(done);
}
