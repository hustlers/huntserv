use std::sync::Arc;
use std::cmp;
use fnv::FnvHashMap as HashMap;
use futures::sync::mpsc::*;
use chrono::*;
use haversine;
use antidote::RwLock;
use message::*;
use error::*;
use client::NotifyID;
use data::DataTrait;
use database::*;

const HIGH_TRAFFIC: f64 = 3.0;

/// An event.
#[derive(Clone, Debug)]
pub enum GameEv {
   /// Send the updated version of the referenced data to all clients.
   DataUpdate(DataType, u32),
   /// Send the updated version of the referenced data to some clients.
   PartialUpdate(Vec<u32>, DataType, u32),
   /// Delete the referenced data.
   DataDelete(DataType, u32),
   /// Send a notification to some group of clients.
   Notify {
      /// The list of players to send the notification to.
      players: Vec<u32>,
      /// The short title of the notification.
      title: String,
      /// The main notification text.
      text: String,
      /// The type of icon to use.
      icon: NotificationIcon,
      /// An ID, notifications with the same ID will overwrite eachother
      id: u32
   },
   /// Start a `game` for the given hunt ID.
   StartHunt(u32),
}

/// The state of a single running game.
#[derive(Clone)]
pub struct SharedGame {
   inner: Arc<RwLock<GameInner>>,
   snd: UnboundedSender<GameEv>
}

/// The internal structure of a `SharedGame`.
struct GameInner {
   log: ::Log,
   /// The ID of the hunt.
   hunt_id: u32,
   /// A reference to the main database.
   db: Arc<Database>,
   /// The set of teams participating in the game.
   teams: HashMap<u32, GameTeam>,
   /// The main set of points to be found for a team to complete the game.
   points: HashMap<u32, GamePoint>,
   /// A map of QR codes to unlockables.
   codes: HashMap<String, Unlockable>,
   /// Map of player IDs to team IDs.
   player_map: HashMap<u32, u32>,
}

/// A team participating in a game.
struct GameTeam {
   log: ::Log,
   /// A list of the team's players.
   players: Vec<u32>,
   /// The team's score
   score: u32,
   /// The next point the team needs to find.
   next_point: u32,
   /// The list of points the team has already found.
   points_found: Vec<(u32, DateTime<UTC>)>,
   /// The list of bonus points found
   bonuses_found: Vec<(u32, DateTime<UTC>)>,
}

/// Something which can be scanned to increase a team's score.
enum Unlockable {
   /// A main point in the treasure hunt.
   Point(u32),
   /// A bonus point which can be collected along the way.
   Bonus(u32),
}

/// One of the main unlockable points in a game.
#[derive(Debug)]
struct GamePoint {
   /// The teams which have already found this point and the time it took to get
   /// there.
   found_by: Vec<(u32, Duration)>,
   /// The IDs of teams currently searching for this point, and the times they
   /// started.
   traffic: Vec<(u32, DateTime<UTC>)>,
   /// The point's location.
   location: (f64, f64),
   /// The relative base difficulty of the location's clue.
   difficulty: f64,
   /// The clue for the point
   clue: String,
}

impl SharedGame {
   /// Create a new `SharedGame`.
   pub fn new(id: u32, log: ::Log, db: Arc<Database>, ev_snd: UnboundedSender<GameEv>) -> SharedGame {
      let game = GameInner {
         log,
         hunt_id: id,
         db,
         teams: HashMap::default(),
         points: HashMap::default(),
         codes: HashMap::default(),
         player_map: HashMap::default(),
      };

      SharedGame {
         inner: Arc::new(RwLock::new(game)),
         snd: ev_snd,
      }
   }

   /// Initialise the game.
   pub fn init(&mut self) -> Result<()> {
      self.inner.write().init(|ev| self.send_ev(ev))
   }

   /// Attempt to unlock a location or bonus point, returning an error if
   /// unsuccessful.
   pub fn unlock(&mut self, player: u32, code: Unlock) -> Result<()> {
      self.inner.write().unlock(player, code, |ev| self.send_ev(ev))
   }

   /// Get the user IDs of all participating players.
   pub fn participants(&self) -> Vec<u32> {
      self.inner.read().participants()
   }

   /// Send a game event to the main service.
   fn send_ev(&self, ev: GameEv) {
      if let Err(e) = self.snd.send(ev) {
         panic!("Failed to send internal event: {}", e);
      }
   }
}

impl GameInner {
   fn hunt_mut(&mut self) -> WriteGuard<Hunt> {
      self.db.hunts.get_mut(self.hunt_id).expect("Missing hunt")
   }

   /// Attempt to unlock a location or bonus point, returning an error if
   /// unsuccessful.
   pub fn unlock<F: Fn(GameEv)>(&mut self, user_id: u32, code: Unlock, send_ev: F) -> Result<()> {
      if let Some(&team_id) = self.player_map.get(&user_id) {

         debug!(self.log, "Got unlock request from {}, '{}'", user_id, code.get_code());

         if code.get_code() == "reset" {
            let mut team = self.teams.remove(&team_id).unwrap();
            for &(point_id, _) in &team.bonuses_found {
               send_ev(GameEv::PartialUpdate(team.players.clone(), DataType::BONUS, point_id));
            }
            team.reset();
            for point in self.points.values_mut() {
               point.remove_team(team_id);
            }
            team.next_point(&self.points, self.target_difficulty(team_id));
            team.write(&mut self.db.teams.get_mut(team_id).unwrap());

            self.teams.insert(team_id, team);
            send_ev(GameEv::DataUpdate(DataType::TEAM, team_id));
            bail!("Your team's points have been reset.");
         }

         match self.codes.get(code.get_code()) {
            Some(&Unlockable::Point(point_id)) => {
               {
                  let team = self.teams.get_mut(&team_id).unwrap();
                  if point_id != team.next_point {
                     if team.has_point(point_id) {
                        bail!("Your team has already found this point!");
                     } else {
                        bail!("This is not the point you're looking for!");
                     }
                  }
               }
               self.found_point(team_id, point_id, send_ev);
               Ok(())
            }
            Some(&Unlockable::Bonus(bonus_id)) => {
               {
                  let team = self.teams.get_mut(&team_id).unwrap();
                  if team.has_bonus(bonus_id) {
                     bail!("Your team has already found this bonus point!");
                  }
               }
               self.found_bonus(team_id, bonus_id, send_ev);
               Ok(())
            }
            None => bail!("Unknown QR code."),
         }
      } else {
         bail!("This player is not part of the hunt.")
      }
   }

   /// Get the user IDs of all participating players.
   fn participants(&self) -> Vec<u32> {
      let mut players = Vec::new();
      for team in self.teams.values() {
         players.extend_from_slice(&team.players);
      }
      players
   }

   /// Get the current scores as an ordered list of `(team_id, score)`.
   fn scores(&self) -> Vec<(u32, u32)> {
      let mut scores = self.teams.iter()
         .map(|(id, team)| (*id, team.score))
         .collect::<Vec<_>>();
      scores.sort_by(|a, b| a.1.cmp(&b.1));
      scores
   }

   /// A team has found a new point
   fn found_point<F: Fn(GameEv)>(&mut self, team_id: u32, point_id: u32, send_ev: F) {
      self.points.get_mut(&point_id).unwrap().found(team_id);

      let target_difficulty = self.target_difficulty(team_id);
      let team = self.teams.get_mut(&team_id).unwrap();
      let mut finished = false;

      team.points_found.push((point_id, UTC::now()));
      team.score += 1;

      send_ev(GameEv::PartialUpdate(team.players.clone(), DataType::POINT, team.next_point));
      if team.points_found.len() == self.points.len() {
         finished = true;
      } else {
         team.next_point(&self.points, target_difficulty);
      }
      team.write(&mut self.db.teams.get_mut(team_id).unwrap());
      send_ev(GameEv::DataUpdate(DataType::TEAM, team_id));

      if finished {
         send_ev(GameEv::Notify {
               players: team.players.clone(),
               title: "You Finished the hunt!".to_string(),
               text: "Your team has found all the points in this treasure hunt".to_string(),
               icon: NotificationIcon::GENERAL,
               id: NotifyID::HuntComplete as u32,
         });
      } else {
         let clue = &self.points.get_mut(&team.next_point).unwrap().clue;
         send_ev(GameEv::Notify {
               players: team.players.clone(),
               title: "Your next clue:".to_string(),
               text: format!("\"{}\"", clue),
               icon: NotificationIcon::GENERAL,
               id: NotifyID::NewClue as u32,
         });
      }
   }

   fn target_difficulty(&self, team_id: u32) -> f64 {
      let scores = self.scores();
      if let Some(ranking) = scores.iter().position(|&(id, _)| id == team_id) {
         let relative_ranking = ranking as f64 / scores.len() as f64;
         1.0 - relative_ranking
      } else  {
         error!(self.log, "Failed to get relative ranking for team {}", team_id);
         0.5
      }
   }


   /// A team has found a new bonus point
   fn found_bonus<F: Fn(GameEv)>(&mut self, team_id: u32, point_id: u32, send_ev: F) {
      println!("wat");
      let team = self.teams.get_mut(&team_id).unwrap();
      team.bonuses_found.push((point_id, UTC::now()));
      team.score += 1;

      team.write(&mut self.db.teams.get_mut(team_id).unwrap());
      send_ev(GameEv::DataUpdate(DataType::TEAM, team_id));
      send_ev(GameEv::PartialUpdate(team.players.clone(), DataType::BONUS, point_id));

      let name = self.db.bonuses.get(point_id).unwrap().get_name().to_string();
      send_ev(GameEv::Notify {
            players: team.players.clone(),
            title: "Bonus point found!".to_string(),
            text: format!("Your team has found {}.", name),
            icon: NotificationIcon::GENERAL,
            id: NotifyID::BonusUnlock as u32,
      });
   }

   /// Initialise the game, loading all the point, teams, and so on.
   fn init<F: Fn(GameEv)>(&mut self, send_ev: F) -> Result<()> {

      info!(self.log, "Initialising hunt {}", self.hunt_id);

      // Load the set of points from the database:

      let mut hunt = self.hunt_mut();

      let points = hunt.get_points().to_vec();
      hunt.set_total_points(points.len() as u32);
      let mut next_point_idx = 0;
      let mut max_difficulty = 0.0f64;

      if points.is_empty() {
         bail!("Can't start a game for a hunt with no points.");
      }

      for &point_id in &points {
         if let Some(point) = self.db.points.get(point_id) {
            let location = (
               point.get_location().get_latitude(),
               point.get_location().get_longitude());

            let difficulty = point.get_difficulty() as f64;
            max_difficulty = max_difficulty.max(difficulty);

            self.points.insert(point_id, GamePoint {
               found_by: Vec::new(),
               traffic: Vec::new(),
               location: location,
               difficulty: difficulty,
               clue: point.get_clue().to_string(),
            });

            self.codes.insert(point.get_password().to_string(), Unlockable::Point(point_id));
         } else {
            bail!("Missing point {}", point_id);
         }
      }

      // Normalise point difficulty:

      for point in self.points.values_mut() {
         point.difficulty /= max_difficulty;
      }

      // Load bonus points:

      for &bonus_id in hunt.get_bonuses() {
         if let Some(bonus) = self.db.bonuses.get(bonus_id) {
            self.codes.insert(bonus.get_password().to_string(), Unlockable::Bonus(bonus_id));
         } else {
            bail!("Missing bonus {}", bonus_id);
         }
      }

      // Reset chat groups:

      for &group_id in hunt.get_chats() {
         if let Some(group) = self.db.chats.get(group_id) {
            for &msg_id in group.get_messages() {
               self.db.messages.delete(msg_id);
               send_ev(GameEv::DataDelete(DataType::CHAT_MESSAGE, msg_id));
            }
         }
         self.db.chats.delete(group_id);
         send_ev(GameEv::DataDelete(DataType::CHAT, group_id));
      }
      hunt.mut_chats().clear();

      // Add the broadcast group:

      let mut broadcast = Chat::new();
      broadcast.new_id(&self.db);
      broadcast.set_group_name("Broadcast".to_string());
      broadcast.set_is_broadcast(true);

      for &user_id in hunt.get_demonstrators() {
         if self.db.users.has(user_id) {
            broadcast.mut_participants().push(user_id);
         } else {
            bail!("Missing demonstrator {}", user_id);
         }
      }

      hunt.mut_chats().push(broadcast.id());
      broadcast.insert_into(&self.db);

      // Load teams from the database:

      for team_id in hunt.get_teams().to_vec() {
         if let Some(mut db_team) = self.db.teams.get_mut(team_id) {
            let next_point = points[next_point_idx];
            next_point_idx += 1;
            if next_point_idx == points.len() { next_point_idx = 0; }

            for &user_id in db_team.get_players() {
               self.player_map.insert(user_id, team_id);
            }

            let log = self.log.new(o!("team_id" => team_id));

            let team = GameTeam {
               log,
               players: db_team.get_players().to_vec(),
               score: 0,
               next_point: next_point,
               points_found: Vec::new(),
               bonuses_found: Vec::new(),
            };

            team.write(&mut db_team);
            self.teams.insert(db_team.id(), team);
            send_ev(GameEv::DataUpdate(DataType::TEAM, db_team.id()));

            self.points.get_mut(&next_point)
               .unwrap()
               .add_team(db_team.id());

            // Add a team chat group:

            let mut chat = Chat::new();
            chat.new_id(&self.db);
            chat.set_group_name(db_team.get_name().to_string());
            chat.set_is_broadcast(false);

            for &user_id in db_team.get_players() {
               if self.db.users.has(user_id) {
                  chat.mut_participants().push(user_id);
               } else {
                  bail!("Missing player {}", user_id);
               }
            }

            hunt.mut_chats().push(chat.id());
            chat.insert_into(&self.db);
         } else {
            bail!("Missing team {}", team_id);
         }
      }

      send_ev(GameEv::DataUpdate(DataType::HUNT, self.hunt_id));

      info!(self.log, "Hunt started {}", self.hunt_id);
      Ok(())
   }
}

impl GamePoint {
   /// Estimate the relative difficulty of finding this point given some starting
   /// location. Returns a value between 0 and 1, where 1 is the most difficult.
   fn difficulty(&self, start: (f64, f64)) -> f64 {
      let distance = self.distance_to(start);
      let relative_distance = (distance / 0.5).min(1.0);

      let relative_time = if let Some(time) = self.avr_duration() {
         (time / (15.0 * 60.0)).min(1.0)
      } else {
         1.0
      };

      self.difficulty * relative_distance * relative_time
   }

   /// Calculate the distance in kilometers between this point and another.
   fn distance_to(&self, end: (f64, f64)) -> f64 {
      let start = self.location;
      let a = haversine::Location { latitude: start.0, longitude: start.1 };
      let b = haversine::Location { latitude: end.0, longitude: end.1 };
      haversine::distance(a, b, haversine::Units::Kilometers)
   }

   /// A relative measure of the current traffic on the point.
   fn traffic(&self) -> f64 {
      if let Some(avr) = self.avr_duration() {
         let high = avr * 2.0;

         self.traffic.iter()
            .map(|team| team.1)
            .map(|start| UTC::now().signed_duration_since(start))
            .map(|time| 1.0 - (time.num_seconds() as f64 / high))
            .map(|x| x.max(0.5))
            .sum()
      } else {
         self.traffic.len() as f64
      }
   }

   /// Calculate the average time in seconds taken to find the point.
   /// Returns `None` if no teams have found it yet.
   fn avr_duration(&self) -> Option<f64> {
      if self.found_by.is_empty() { return None; }
      Some(self.found_by
           .iter()
           .map(|team| team.1.num_seconds() as f64)
           .sum::<f64>() / self.found_by.len() as f64)
   }

   /// Start tracking the time for a new team to find this point.
   fn add_team(&mut self, team_id: u32) {
      self.traffic.push((team_id, UTC::now()));
   }

   /// Remove a team.
   fn remove_team(&mut self, team_id: u32) {
      if let Some(index) = self.traffic.iter().position(|p| p.0 == team_id) {
         self.traffic.swap_remove(index);
      }
      if let Some(index) = self.found_by.iter().position(|p| p.0 == team_id) {
         self.found_by.swap_remove(index);
      }
   }

   /// A team has found this point -- returns the time taken, or `None` if we
   /// don't have a start time for the given team.
   fn found(&mut self, team_id: u32) -> Option<Duration> {
      if let Some(index) = self.traffic.iter().position(|p| p.0 == team_id) {
         let start = self.traffic.swap_remove(index).1;
         Some(UTC::now().signed_duration_since(start))
      } else {
         None
      }
   }
}

impl GameTeam {

   /// Check if the team has found the point with the given ID
   fn has_point(&self, point_id: u32) -> bool {
      self.points_found.iter().any(|p| p.0 == point_id)
   }

   /// Check if the team has found the point with the given ID
   fn has_bonus(&self, point_id: u32) -> bool {
      self.bonuses_found.iter().any(|p| p.0 == point_id)
   }

   /// Reset the team's points.
   fn reset(&mut self) {
      self.score = 0;
      self.points_found.clear();
      self.bonuses_found.clear();
   }

   /// Write the team to the database value.
   fn write(&self, team: &mut WriteGuard<Team>) {
      team.set_score(self.score);
      team.set_bonuses_found(self.bonuses_found.len() as u32);
      team.set_points_found(self.points_found.len() as u32);

      let mut done = false;

      let mut points: Vec<_> = self.points_found.iter()
         .map(|&(id, time)| {
            let mut p = Team_Point::new();
            if id == self.next_point { done = true; }
            p.set_id(id);
            p.set_time_found(time.timestamp() as u64);
            p
         })
         .collect();

      let bonuses: Vec<_> = self.bonuses_found.iter()
         .map(|&(id, _)| id )
         .collect();

      if !done {
         warn!(self.log, "next_point in points_found, not sending next_point..");
         let mut p = Team_Point::new();
         p.set_id(self.next_point);
         points.push(p);
      }

      debug!(self.log, "a team: {:?}", **team);

      team.set_bonuses(bonuses);
      team.set_points(::protobuf::RepeatedField::from_vec(points));
   }

   fn next_point(&mut self, point_set: &HashMap<u32, GamePoint>, target_difficulty: f64) {
      if self.points_found.len() == point_set.len() {
         warn!(self.log, "next_point when all points are found!");
         return;
      }
      self.next_point = self.pick_next_point(point_set, target_difficulty);
   }

   fn pick_next_point(&mut self, point_set: &HashMap<u32, GamePoint>, target_difficulty: f64) -> u32 {
      let current_pos = point_set.get(&self.next_point).unwrap().location;

      debug!(self.log, "Looking for a new point, target difficulty: {}", target_difficulty);

      // Filter out points we've already found and points with too high traffic.
      let available = point_set.iter()
         .map(|(id, point)| (*id, point))
         .filter(|&(id, _)| !self.points_found.iter().map(|p| p.0).any(|found_id| found_id == id))
         .collect::<Vec<(u32, &GamePoint)>>();

      let traffic_filtered = available.iter()
         .cloned()
         .filter(|&(_,point)| point.traffic() <= HIGH_TRAFFIC)
         .collect::<Vec<(u32, &GamePoint)>>();

      debug!(self.log, "Available points: {:?}", available);
      debug!(self.log, "Traffic filtered points: {:?}", traffic_filtered);

      let mut points = if traffic_filtered.is_empty() {
         available
      } else {
         traffic_filtered
      };

      let target_elem = cmp::min(
         points.len() - 1,
         (target_difficulty * (points.len() as f64 - 1.0 as f64)) as usize);
      let mut elem = 0;

      debug!(self.log, "Looking for the {}(st/nd/th) least difficult point", target_elem + 1);

      loop {
         let mut min_index = 0;
         let mut min = 1.0;

         for (i, point) in points.iter().enumerate() {
            let difficulty = point.1.difficulty(current_pos);
            if difficulty < min {
               min = difficulty;
               min_index = i;
            }
         }

         let point = points.swap_remove(min_index);
         if elem == target_elem { return point.0; }
         elem += 1;
      }
   }
}

#[test]
fn pick_point_test() {
   let mut point_set = HashMap::default();
   point_set.insert(0, GamePoint {
         found_by: vec![],
         traffic: vec![],
         location: (0.0, 0.0),
         difficulty: 0.5,
         clue: "".to_string(),
      });

   point_set.insert(1, GamePoint {
         found_by: vec![],
         traffic: vec![],
         location: (0.0, 0.0),
         difficulty: 0.5,
         clue: "".to_string(),
      });

   let mut team = GameTeam {
      log: ::logger(),
      players: vec![],
      score: 0,
      next_point: 0,
      points_found: vec![(0, UTC::now())],
      bonuses_found: vec![],
   };

   team.next_point(&point_set, 1.0);
   assert_eq!(1, team.next_point);
}
