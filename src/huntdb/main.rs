/// Creates a test database from some data defined in JSON.

extern crate huntserv;
#[macro_use] extern crate slog;
extern crate bcrypt;
extern crate rand;
extern crate serde;
extern crate serde_json;

use huntserv::database::*;
use huntserv::test_data::*;
use std::fs::File;

fn main() {
   let log = huntserv::logger();
   let db_root = "db";
   let db = Database::new(db_root, log.clone());
   let mut users = Vec::new();

   let file = File::open("data.json").unwrap();
   let hunt: Hunt = serde_json::from_reader(file).unwrap();

   hunt.to_db(&db, &mut users).unwrap();

   for user in users {
      info!(log, "Name: {}, Username: {}, Password: {}",
            user.name, user.username.unwrap(), user.password.unwrap());
   }

   db.save().unwrap();
}
