use fnv::FnvHashMap as HashMap;
use fnv::FnvHashSet as HashSet;
use message::DataType;

/// A bi-directional multimap between clients and data IDs, used for tracking
/// what data each client knows of.
#[derive(Debug, Default)]
pub struct ListenMap {
   /// Maps data to the IDs of clients which want that data.
   map: HashMap<(DataType, u32), HashSet<u64>>,
   /// Maps clients to their set of IDs
   clients: HashMap<u64, HashSet<(DataType, u32)>>,
}

/// A view into a `ListenMap` for a particular client.
pub struct Listens<'a> {
   map: &'a mut HashMap<(DataType, u32), HashSet<u64>>,
   set: &'a mut HashSet<(DataType, u32)>,
   id: u64,
}

impl<'a> Listens<'a> {
   /// Check if this client already has some data.
   pub fn has(&self, kind: DataType, id: u32) -> bool {
      self.set.contains(&(kind, id))
   }

   /// Add some data to the map.
   pub fn add(&mut self, kind: DataType, id: u32) {
      self.set.insert((kind, id));
      self.map.entry((kind, id))
         .or_insert_with(HashSet::default)
         .insert(self.id);
   }
}

impl ListenMap {
   /// Get thet set of clients who are listening for some data.
   pub fn set(&mut self, kind: DataType, id: u32) -> &HashSet<u64> {
      self.map.entry((kind, id))
         .or_insert_with(HashSet::default)
   }

   /// Insert a value into the map
   pub fn insert(&mut self, kind: DataType, id: u32, client_id: u64) {
      self.map.entry((kind, id))
         .or_insert_with(HashSet::default)
         .insert(client_id);

      self.clients.entry(client_id)
         .or_insert_with(HashSet::default)
         .insert((kind, id));
   }

   /// Remove a value from the map
   pub fn remove(&mut self, kind: DataType, id: u32, client_id: u64) {
      let mut empty = false;
      if let Some(set) = self.map.get_mut(&(kind, id)) {
         set.remove(&client_id);
         empty = set.is_empty()
      }
      if empty { let _ = self.map.remove(&(kind, id)); }
      empty = false;

      if let Some(set) = self.clients.get_mut(&client_id) {
         set.remove(&(kind, id));
         empty = set.is_empty()
      }
      if empty { let _ = self.clients.remove(&client_id); }
   }

   /// Remove all client's listens for some value.
   pub fn remove_data(&mut self, kind: DataType, id: u32) {
      if let Some(clients) = self.map.remove(&(kind, id)) {
         for c in clients {
            self.clients.get_mut(&c).unwrap().remove(&(kind, id));
         }
      }
   }

   /// Get a `Listnes` struct for the given client.
   pub fn listens(&mut self, client_id: u64) -> Listens {
      Listens {
         map: &mut self.map,
         set: self.clients.entry(client_id).or_insert_with(HashSet::default),
         id: client_id,
      }
   }

   /// Remove all listens for the client with the given ID.
   pub fn remove_client(&mut self, client_id: u64) {
      if let Some(client) = self.clients.remove(&client_id) {
         for (kind, id) in client {
            self.remove(kind, id, client_id);
         }
      }
   }

   /// Update the client ID for some client.
   pub fn resume_client(&mut self, old_id: u64, new_id: u64) {
      if let Some(client) = self.clients.remove(&old_id) {
         for &(kind, id) in &client {
            self.remove(kind, id, old_id);
            self.insert(kind, id, new_id);
         }
      }
   }
}
