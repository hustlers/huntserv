//! The main server binary. Creates a socket and service and passes messages
//! between them.

#![feature(type_ascription)]

#[macro_use] extern crate slog;
#[macro_use] extern crate error_chain;
extern crate dotenv;
extern crate huntserv;
extern crate tokio_signal;
extern crate tokio_core;
extern crate tokio_timer;
extern crate futures;

use std::env;
use std::net::SocketAddr;
use std::time::*;
use futures::*;
use futures::sync::mpsc;
use tokio_core::reactor::Core;
use tokio_timer::*;
use huntserv::service::*;
use huntserv::socket::*;
use error::*;

/// The time in miliseconds between looking for and removing old clients.
const CLEAN_TIME: u64 = 5_000;

const UPDATE_TIME: u64 = 200;

/// Basic configuration, loaded from environmental variables.
struct ServerConfig {
   /// The time in milliseconds between saving the database.
   save_time: u64,
   /// An optional password for an initial admin account with which to create new users.
   admin_pw: Option<String>,
   /// The path to use for loading/saving the database.
   db_path: String,
   /// The path to the key archive containing the SSL key and certificate chain.
   key_path: String,
   /// The password to the key archive
   key_pw: String,
   /// The address to bind to for plain TCP connections.
   tcp_addr: SocketAddr,
   /// The address to bind to for WebSocket connections.
   ws_addr: SocketAddr,
}

// We share the `Service` across multiple closures using a ref-counted pointer,
// this macro makes it easy to clone that pointer into each closure.
macro_rules! clone {
   ($($n:ident),+; |$($p:pat),+| $body:block) => ({
      $( let $n = $n.clone(); )+
      move |$($p),+| { $body }
   });
}

fn main() {
   let log = huntserv::logger();

   info!(log, "Loading env configuration");
   // Load the server configuration from environmental variables.
   let config = ServerConfig::from_env().unwrap_or_else(|e| {
      error!(log, "Failed to load configuration: {}", e);
      panic!()
   });

   info!(log, "Loading key archive");
   let key_archive = pkcs12_from_file(&config.key_path, &config.key_pw).unwrap_or_else(|e| {
      error!(log, "Failed to load key archive: {}", e);
      panic!()
   });

   // Create the socket configuration.
   let socket_config = SocketConfig {
      log: log.clone(),
      tcp_addr: config.tcp_addr,
      ws_addr: config.ws_addr,
      key_archive: key_archive,
   };

   info!(log, "Creating core");
   // Create a 'reactor core', an event loop on which futures run.
   let mut core = Core::new().unwrap();
   let timer = Timer::default();

   // Create a channel for internal events.
   let (game_snd, game_rec) = mpsc::unbounded();

   info!(log, "Starting sockets");
   // Start listening on the sockets, returns a receiver on which we get events.
   let rec = listen(socket_config, &core.handle()).unwrap_or_else(|e| {
      error!(log, "Failed to start sockets: {}", e);
      panic!();
   });

   info!(log, "Starting service");
   // Create the service, which handles all the server logic.
   let service = Service::new(&config.db_path, log.clone(), game_snd);
   info!(log, "Adding admin account");
   service.setup_admin(config.admin_pw).unwrap();
   let service = ::std::rc::Rc::new(::std::cell::RefCell::new(service));

   // For all incoming events, pass the event to the service.
   let incoming = rec
      .for_each(clone!(service; |ev| {
         service.borrow_mut().handle(ev);
         Ok(())
      }));

   // Pass all internal events back to the service.
   let game_evs = game_rec
      .for_each(clone!(service; |ev| {
         service.borrow_mut().handle_game(ev);
         Ok(())
      }));

   // Start an interval timer for regularly saving the database.
   let save = timer.interval(Duration::from_millis(config.save_time))
      .for_each(clone!(service; |()| {
         service.borrow_mut().save();
         Ok(())
      })).map_err(|e| panic!("{}", e));

   // Start an interval timer for regularly cleaning old connections.
   let clean = timer.interval(Duration::from_millis(CLEAN_TIME))
      .for_each(clone!(service; |()| {
         service.borrow_mut().clean();
         Ok(())
      })).map_err(|e| panic!("{}", e));

   // Start an interval timer for updating client data
   let update = timer.interval(Duration::from_millis(UPDATE_TIME))
      .for_each(clone!(service; |()| {
         service.borrow_mut().update();
         Ok(())
      })).map_err(|e| panic!("{}", e));

   // Save the database and exit on Ctrl+C
   let signal = tokio_signal::ctrl_c(&core.handle())
      .map_err(|e| e.into(): Error)
      .and_then(clone!(service, log; |stream| {
         stream
            .map_err(|e| e.into(): Error)
            .for_each(move |_| {
               info!(log, "Received a signal");
               service.borrow_mut().stop();
               // Return an error so that the future resolves and `core.run`
               // returns, ending the program.
               bail!("done")
            })
      }));

   // Spawn each future onto the event loop.
   info!(log, "Starting");
   let handle = core.handle();
   handle.spawn(game_evs);
   handle.spawn(clean);
   handle.spawn(save);
   handle.spawn(update);
   handle.spawn(incoming);

   // Run the `signal` future, which completes after hadling a close signal,
   // exiting the program.
   let _ = core.run(signal);
}

impl ServerConfig {
   /// Load the configuration from environmental variables
   fn from_env() -> Result<ServerConfig> {
      // Load any missing values from a `.env` file.
      let _ = dotenv::dotenv();
      let addr_str = env::var("BIND_ADDRESS")?;
      let ws_addr_str = env::var("WS_ADDRESS")?;
      let save_time_str = env::var("SAVE_TIME")?;

      Ok(ServerConfig {
         admin_pw: env::var("ADMIN_PASSWORD").ok(),
         db_path: env::var("DATABASE_PATH")?,
         key_path: env::var("KEY_ARCHIVE")?,
         key_pw: env::var("KEY_ARCHIVE_PASSWORD")?,
         save_time: save_time_str.parse()?,
         tcp_addr: addr_str.parse()?,
         ws_addr: ws_addr_str.parse()?,
      })
   }
}

// An error type for the various errors encountered starting the server.
mod error {
   error_chain! {
      foreign_links {
         IO(::std::io::Error);
         Env(::std::env::VarError);
         AddrParse(::std::net::AddrParseError);
         IntParse(::std::num::ParseIntError);
      }
   }
}
