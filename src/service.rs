use fnv::FnvHashMap as HashMap;
use std::sync::Arc;
use std::time::*;
use futures::sync::mpsc::*;
use bcrypt;

use database::Database;
use error::*;
use game::*;
use listen::*;
use client::*;
use socket::SocketEv;
use connection::*;
use message::*;

/// The time in seconds for which to keep a client's state after they disconnect.
pub const SESSION_TIME: u64 = 2 * 60 * 60;

/// Provides service to multiple clients -- tracks connections and sessions for
/// clients, passes all incoming messages to the relevant `Connection` or `Client`.
pub struct Service {
   log: ::Log,
   /// A shared reference to the data store.
   db: Arc<Database>,
   /// Map of connecion IDs to connections, for clients who are not yet logged in.
   connections: HashMap<u64, Connection>,
   /// Map of connection IDs to clients.
   clients: HashMap<u64, Client>,
   /// Map of session IDs to current connection IDs
   sessions: HashMap<u128, u64>,
   /// Map of connection IDs to the time the connection was closed.
   disconnected: HashMap<u64, Instant>,
   /// Maps from player IDs to connection IDs.
   id_map: HashMap<u32, u64>,
   /// Map of data IDs to sets of client IDs who are watching that data.
   listen_map: ListenMap,
   /// Map of hunt IDs to active games
   games: HashMap<u32, SharedGame>,
   /// A channel on which internal events are sent between clients.
   ev_snd: UnboundedSender<GameEv>,
   ping_counter: usize,
   ping_idle: usize,
}

impl Service {
   /// Create a new service
   pub fn new(db_root: &str, log: ::Log,
              ev_snd: UnboundedSender<GameEv>) -> Service
   {
      let db = Arc::new(Database::load(db_root, log.clone()).unwrap());

      Service {
         log, db, ev_snd,
         connections: HashMap::default(),
         sessions: HashMap::default(),
         clients: HashMap::default(),
         disconnected: HashMap::default(),
         id_map: HashMap::default(),
         listen_map: ListenMap::default(),
         games: HashMap::default(),
         ping_counter: 0,
         ping_idle: 0,
      }
   }

   /// Handle an incoming event.
   pub fn handle(&mut self, ev: SocketEv) {
      match ev {
         // A new client has connected.
         SocketEv::NewClient(id, snd) => {
            let log = self.log.new(o!("client_id" => id));
            self.connections.insert(id, Connection::new(log, id, snd));
         },

         // A client sent us a message.
         SocketEv::Message(id, msg) => {

            // If we already know about this client, forward the message.
            if let Some(client) = self.clients.get_mut(&id) {
               client.handle(msg);
               return;
            }

            // Try to get the connection.
            if !self.connections.contains_key(&id) {
               error!(self.log, "Missing connection for client {}!", id);
               return;
            }
            let mut conn = self.connections.remove(&id).unwrap();

            match conn.handle(msg) {
               Some(MessageData::resume(session)) => self.resume(conn, session),
               Some(MessageData::login_request(req)) => self.login(conn, req),
               Some(x) => warn!(self.log, "Unexpected message from {:?}: {:?}", id, x),
               _ => (),
            }
         }

         // A client has disconnected.
         SocketEv::Shutdown(id) => {
            if self.clients.contains_key(&id) {
               self.clients.get_mut(&id).unwrap().disconnected();
               self.disconnected.insert(id, Instant::now());
            }
            if let Some(_c) = self.connections.remove(&id) { }
         }
      }
   }

   fn send_client_data(&mut self, client_id: u64, kind: DataType, id: u32) {
      if let Some(client) = self.clients.get_mut(&client_id) {
         debug!(self.log, "sending update for {:?} {} to client {}", kind, id, client_id);
         if let Err(e) = client
            .follow_tree(kind, id, &mut self.listen_map.listens(client_id)) {
               warn!(self.log, "Client error on DataUpdate: {}", e);
         }
         client.send_updates();
      } else {
         error!(self.log, "Invalid client ID in listen map: {} \n {:?}", client_id, self.listen_map);
      }
   }

   /// Handle an internal event from a client.
   pub fn handle_game(&mut self, ev: GameEv) {
      match ev {
         GameEv::DataUpdate(kind, id) => {
            for client_id in self.listen_map.set(kind, id).clone() {
               self.send_client_data(client_id, kind, id);
            }
         }
         GameEv::PartialUpdate(users, kind, id) => {
            let client_set = self.listen_map.set(kind, id).clone();
            for user_id in users {
               if let Some(&client_id) = self.id_map.get(&user_id) {
                  if client_set.contains(&client_id) {
                     self.send_client_data(client_id, kind, id);
                  }
               }
            }
         }
         GameEv::DataDelete(kind, id) => {
            self.listen_map.remove_data(kind, id);
         }
         GameEv::StartHunt(id) => {
            let log = self.log.new(o!("hunt_id" => id));
            let mut game = SharedGame::new(id, log, self.db.clone(), self.ev_snd.clone());

            if let Err(e) = game.init() {
               error!(self.log, "Failed to start game: {}", e);
               return;
            };

            for user_id in game.participants() {
               if let Some(client) = self.client_by_user(user_id) {
                  client.join_game(game.clone());
                  client.send_updates();
               }
            }
            self.games.insert(id, game);
         }
         GameEv::Notify { players, title, text, icon, id } => {
            for user_id in players {
               if let Some(client) = self.client_by_user(user_id) {
                  client.notify(title.clone(), text.clone(), icon, id);
               }
            }
         }
      }
   }

   /// Send any updates to clients.
   pub fn update(&mut self) {
      let target_pings: f64 = self.clients.len() as f64 / 100.0;
      if self.ping_counter >= self.clients.len() {
         self.ping_counter = 0;
         if target_pings < 1.0 {
            self.ping_idle = ((1.0 - target_pings) * 100.0) as usize;
         }
      }
      let mut target_pings = target_pings as usize;

      if self.ping_idle > 0 {
         self.ping_idle -= 1;
      } else {
         target_pings += 1;
      }

      let mut pings = 0;
      let mut i = 0;

      for client in self.clients.values_mut() {
         i += 1;
         if pings < target_pings && i > self.ping_counter {
            pings += 1;
            self.ping_counter += 1;
            client.ping();
         }
         client.send_updates();
      }
   }

   /// Get a client given a user ID.
   fn client_by_user(&mut self, user_id: u32) -> Option<&mut Client> {
      if let Some(client_id) = self.id_map.get(&user_id) {
         self.clients.get_mut(client_id)
      } else {
         None
      }
   }

   /// Close the connection to any client who hasn't acknowledged recent messages.
   /// Remove any session state for clients which have been disconnected for
   /// longer than SESSION_TIME.
   pub fn clean(&mut self) {
      let now = Instant::now();
      let timeout = Duration::from_secs(SESSION_TIME);
      let mut to_remove = Vec::new();

      for c in self.clients.values_mut() {
         c.connection().clean(now);
      }
      for c in self.connections.values() { c.clean(now); }

      for (&id, &time) in &self.disconnected {
         if (now - time) > timeout {
            to_remove.push(id);
         }
      }

      for id in to_remove {
         self.remove_client(id);
      }
   }

   /// Save the database to the filesystem.
   pub fn save(&self) {
      if let Err(e) = self.db.save_changed() {
         error!(self.log, "Failed to save database: {}", e);
      }
   }

   /// Add a new client.
   fn insert_client(&mut self, client: Client) {
      let id = client.id();
      self.sessions.insert(client.session_id(), id);
      self.id_map.insert(client.user_id(), id);
      self.clients.insert(id, client);
   }

   /// Remove a client's state and any references to it.
   fn remove_client(&mut self, id: u64) {
      self.listen_map.remove_client(id);
      self.disconnected.remove(&id);
      if let Some(c) = self.clients.remove(&id) {
         //c.connection().shutdown();
         let _ = self.id_map.remove(&c.user_id());
         let _ = self.sessions.remove(&c.session_id());
      }
   }

   /// Handle a request to resume a session
   fn resume(&mut self, mut conn: Connection, session: Session) {
      if let Ok(sid) = u128::from_str_radix(session.get_session_id(), 16) {
         if let Some(&client_id) = self.sessions.get(&sid) {
            // Take the client and remove any references to it.
            let mut client = self.clients.remove(&client_id).unwrap();
            self.id_map.remove(&client.user_id());
            self.sessions.remove(&client.session_id());

            // Attempt to resume the session
            match client.resume(conn) {
               None => {
                  // The connection resumed successfully
                  // We re-add the client to our maps.
                  self.listen_map.resume_client(client_id, client.id());
                  self.insert_client(client);
                  return;
               }
               Some(failed) => {
                  self.listen_map.remove_client(client_id);
                  conn = failed;
               }
            }
         }
      }

      let mut msg = Message::new();
      msg.set_request_seq(conn.last_seq());
      conn.send(fail(msg, &"Failed to resume session"));
      self.connections.insert(conn.id(), conn);
   }

   /// Handle a login request from a new client.
   fn login(&mut self, mut conn: Connection, mut req: Login) {
      let id = conn.id();
      let mut msg = Message::new();
      msg.set_request_seq(conn.last_seq());

      let err = match self.verify(&req.take_username(), &req.take_password()) {
         None => "Incorrect username or password",
         Some(user_id) => {
            // If we have a previous session for this client, remove it.
            if let Some(&old_id) = self.id_map.get(&user_id) {
               self.remove_client(old_id);
            }
            match self.db.users.get(user_id) {
               Some(user) => {
                  msg.set_result(ReqResult::new());
                  conn.send(msg);
                  self.new_client(user.clone(), conn);
                  return;
               }
               None => {
                  error!(self.log, "User doesn't exist {}", user_id);
                  "Server error :("
               }
            }
         }
      };

      conn.send(fail(msg, &err));
      self.connections.insert(id, conn);
   }

   fn verify(&self, username: &str, pass: &str) -> Option<u32> {
      self.db.login.find(|x| x.get_username() == username)
         .and_then(|data| bcrypt::verify(pass, data.get_password()).ok()
                   .and_then(|valid| if valid { Some(data.get_user_id()) } else { None }))
   }

   /// Setup an admin account
   pub fn setup_admin(&self, pass: Option<String>) -> Result<()> {
      if pass.is_none() { return Ok(()); }

      let id = self.db.login.find(|x| x.get_username() == "admin")
         .map(|login| login.get_user_id())
         .unwrap_or_else(|| self.db.users.next_id());

      let mut login = LoginDetails::new();
      login.set_user_id(id);
      login.set_username("admin".to_string());
      login.set_password(bcrypt::hash(&(pass.unwrap()), 6)?);
      self.db.login.insert(login);

      let mut user = User::new();
      user.set_id(id);
      user.set_name("Administrator".to_string());
      user.set_permission(Permission::ADMIN);
      self.db.users.insert(user);

      Ok(())
   }

   /// Shutdown the server.
   pub fn stop(&mut self) {
      for c in self.clients.values_mut() {
         c.shutdown();
      }
      info!(self.log, "Saving database");
      self.save();
   }

   /// Create a client for the given user.
   fn new_client(&mut self, user: User, conn: Connection) {
      let id = conn.id();
      let user_id = user.get_id();

      let log = self.log.new(o!("client_id" => id, "user_id" => user_id));

      let mut client = Client::new(log.clone(), user, conn, self.db.clone(), self.ev_snd.clone());

      if let Some(hunt_id) = client.hunt_id() {
         if let Some(game) = self.games.get(&hunt_id).cloned() {
            client.join_game(game);
         }
      }

      client.init(self.listen_map.listens(id));
      client.send_updates();
      client.login_complete();

      self.insert_client(client);
   }
}

fn fail<S: ToString>(mut msg: Message, error: &S) -> Message {
   let mut resp = ReqResult::new();
   resp.set_error(error.to_string());
   msg.set_result(resp);
   msg
}
