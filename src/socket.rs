
use std::sync::atomic::*;
use std::sync::Arc;
use std::net::SocketAddr;
use std::fs::File;
use std::io::Read;
use futures::sync::mpsc;
use futures::*;
use tokio_core::net::{TcpListener, TcpStream};
use tokio_core::reactor::Handle;
use tokio_io::*;
use native_tls::{Pkcs12, TlsAcceptor, TlsConnector};
use tokio_tls::*;
use tokio_io::codec::*;
use tokio_tungstenite;
use bytes::*;

use error::*;
use message;

/// Socket configuration
pub struct SocketConfig {
   /// Some logger.
   pub log: ::Log,
   /// The address to bind to for plain TCP connections.
   pub tcp_addr: SocketAddr,
   /// The address to bind to for WebSocket connections
   pub ws_addr: SocketAddr,
   /// An archive containing the SSL key and certificates.
   pub key_archive: Pkcs12,
}

/// Client configuration
pub struct ClientConfig {
   /// Some logger.
   pub log: ::Log,
   /// The address of the server to connect to.
   pub server_addr: SocketAddr,
   /// The domain of the server for testing it's certificate.
   pub server_domain: String,
}

/// An event sent to a client.
#[derive(Debug)]
pub enum ClientEv {
   /// Send the given message to the client
   Message(message::Message),
   /// Shutdown the connection.
   Shutdown,
}

/// An incoming event.
#[derive(Debug)]
pub enum SocketEv {
   /// A new client has connected, this gives their ID and a channel on which
   /// to send the client messages.
   NewClient(u64, mpsc::UnboundedSender<ClientEv>),
   /// We recieved a message from a client.
   Message(u64, message::Message),
   /// A client has disconnected.
   Shutdown(u64),
}

/// Write a `Message` to the given buffer.
pub fn encode(msg: &message::Message, buf: &mut Vec<u8>) -> Result<()> {
   use protobuf::Message;
   Ok(msg.write_to_vec(buf)?)
}

/// Read a `Message` from some data.
pub fn decode(buf: &[u8]) -> Result<message::Message> {
   use protobuf::parse_from_bytes;
   Ok(parse_from_bytes(buf)?)
}

/// Read a PKCS #12 archive from a file.
pub fn pkcs12_from_file(file_name: &str, password: &str) -> Result<Pkcs12> {
   let mut file = File::open(file_name)?;
   let mut pkcs12 = Vec::new();
   file.read_to_end(&mut pkcs12)?;
   Ok(Pkcs12::from_der(&pkcs12, password)?)
}

// A macro for passing cloned values into a `move` closure.
macro_rules! clone {
   ($($n:ident),+; |$($p:pat),+| $body:block) => ({
      $( let $n = $n.clone(); )+
      move |$($p),+| { $body }
   });
}

/// A stream which implements the treasure hunt protocol.
pub struct HuntStream {
   stream: Box<Stream<Item=message::Message, Error=Error>>,
   sink: Box<Sink<SinkItem=message::Message, SinkError=Error>>,
}

/// Connect to a server
pub fn connect(config: ClientConfig, handle: &Handle) -> Box<Future<Item=HuntStream, Error=Error>> {
   let socket = TcpStream::connect(&config.server_addr, handle);
   let cx = TlsConnector::builder().unwrap().build().unwrap();

   let handshake = socket
      .map_err(|e| e.into())
      .and_then(move |socket| {
         cx.connect_async(&config.server_domain, socket).map_err(|e| e.into())
      });

   Box::new(handshake
      .and_then(|stream| {
         let (sink, stream) = stream.framed(Framing::new()).split();
         Ok(HuntStream{
            stream: Box::new(stream),
            sink: Box::new(sink.buffer(32)),
         })
      }))
}

impl Stream for HuntStream {
   type Item = message::Message;
   type Error = Error;
   fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
      self.stream.poll()
   }
}

impl Sink for HuntStream {
   type SinkItem = message::Message;
   type SinkError = Error;
   fn start_send(&mut self, item: Self::SinkItem) -> StartSend<Self::SinkItem, Self::SinkError> {
      self.sink.start_send(item)
   }
   fn poll_complete(&mut self) -> Poll<(), Self::SinkError> {
      self.sink.poll_complete()
   }
}

/// Start listening for connections.
/// Returns the receiving end of a channel of events, which can be new clients,
/// messages from existing clients, or notifications of a client disconnecting.
/// For new clients the event contains a sender to send messages to the client
/// or drop the connection.
/// `config` contains basic server configuration values.
/// `handle` is a handle to a tokio reactor -- an event loop on which we spawn
/// futures for accepting and handling clients.
pub fn listen(config: SocketConfig, handle: &Handle) -> Result<mpsc::UnboundedReceiver<SocketEv>> {
   // An internal channel on which we pass messages to the service.
   let (in_snd, in_rec) = mpsc::unbounded();

   info!(config.log, "Creating TLS Acceptor");
   let tls_acceptor = TlsAcceptor::builder(config.key_archive)?.build()?;
   let tls_acceptor = Arc::new(tls_acceptor);

   // An atomic value for generating client IDs which are unique across
   // both sockets.
   let ids = Arc::new(AtomicU64::new(0));
   let log = config.log;

   // Create the sockets:
   info!(log, "Binding plain TCP socket");
   let socket = TcpListener::bind(&config.tcp_addr, handle)?;

   info!(log, "Binding WS socket");
   let socket_ws = TcpListener::bind(&config.ws_addr, handle)?;

   // Convert the sockets into streams of incoming connections, logging any
   // errors. When the futures are spawned onto the event loop any errors are
   // dropped, so if one socket fails the other will continue, and existing
   // connection will stay open.

   let ws_clients = socket_ws.incoming()
      .map_err(clone!(log; |e| {
         error!(log, "socket_ws stream error: {}", e);
      }));

   let tcp_clients = socket.incoming()
      .map_err(clone!(log; |e| {
         error!(log, "socket stream error: {}", e);
      }));

   // For each connection on either of the streams, we start the TLS handshake,
   // frame the data into discrete messages, en/decode the messages with protobuf,
   // then pass the resulting future to handle_client, which returns a new
   // future to resolve when the connection is over.
   // We spawn this future on the event loop for it to run to completion.

   let ws_done = ws_clients
      .for_each(clone!(handle, log, in_snd, ids, tls_acceptor; |(stream, addr)| {
         info!(log, "Accepting a WS connection from {}", addr);
         let tls_handshake = tls_acceptor.accept_async(stream)
            .map_err(clone!(log; |e| {
               error!(log, "TLS handshake failed: {}", e);
            }));

         let ws_handshake = tls_handshake
            .and_then(clone!(log; |stream| {
                tokio_tungstenite::accept_async(stream)
                  .map_err(clone!(log; |e| {
                     error!(log, "WebSocket handshake failed: {}", e);
                  }))
            }));

         let done = ws_handshake
            .and_then(clone!(log, in_snd, ids; |stream| {
               let (sink, stream) = stream.split();

               let stream = stream.map_err(|e| e.into(): Error)
                  .and_then(|msg| {
                     decode(&msg.into_data())
                   });

               let sink = sink.sink_map_err(|e| e.into(): Error)
                  .with(|msg: message::Message| {
                     let mut vec = Vec::new();
                     encode(&msg, &mut vec)?;
                     Ok(::tungstenite::Message::Binary(vec))
                  });

               handle_client(stream, sink, &in_snd, &log, addr, ids)
            }));

         handle.spawn(done);
         Ok(())
      }));

   let tcp_done = tcp_clients
      .for_each(clone!(handle, log, in_snd, ids, tls_acceptor; |(stream, addr)| {
         info!(log, "Accepting a TCP connection from {}", addr);

         let tls_handshake = tls_acceptor.accept_async(stream)
            .map_err(clone!(log; |e| {
               error!(log, "TLS handshake failed: {}", e);
            }));

         let done = tls_handshake
            .and_then(clone!(log, in_snd, ids; |stream| {
               let (sink, stream) = stream.framed(Framing::new()).split();
               handle_client(stream, sink, &in_snd, &log, addr, ids)
            }));

         handle.spawn(done);
         Ok(())
      }));

   handle.spawn(ws_done);
   handle.spawn(tcp_done);

   Ok(in_rec)
}

/// Handle a single client, assigning them a new ID, sending the `NewClient` event
/// to the service, then facilitating communication between the client and the
/// service.
fn handle_client<S, K>(
   stream: S,
   mut sink: K,
   in_snd: &mpsc::UnboundedSender<SocketEv>,
   log: &::Log,
   addr: SocketAddr,
   ids: Arc<AtomicU64>) -> BoxFuture<(),()>
   where S: Stream<Item=message::Message, Error=Error> + Send + 'static,
         K: Sink<SinkItem=message::Message, SinkError=Error> + Send + 'static
{
   // Generate a new unique ID for the client.
   let id = ids.fetch_add(1, Ordering::Relaxed);

   // Create a logger with the client's ID
   let log = log.new(o!("client_id" => id));
   info!(log, "New client connected from {:?}", addr);

   // Create a channel for the service to send us events.
   let (client_snd, client_rec) = mpsc::unbounded();

   // Send this channel along with the ID to the service.
   let res = in_snd.send(SocketEv::NewClient(id, client_snd.clone()));

   if let Err(e) = res {
      error!(log, "Failed to send on internal channel! {}", e);
      panic!();
   }

   let incoming = stream.for_each(clone!(in_snd; |msg| {
         // Forward every message we receive from the client to the service.
         (&in_snd).send(SocketEv::Message(id, msg))
            .map_err(|_| "Failed to send on internal channel")?;
         Ok(())
      }))
      .then(clone!(log, in_snd; |res| {
         match res {
            Ok(_) => info!(log, "Connection closed"),
            Err(e) => error!(log, "Connection error: {}", e),
         }
         // Tell the service that the connection is closed.
         let _ = (&in_snd).send(SocketEv::Shutdown(id));
         Ok(())
      }));

   let mut open = true;
   let outgoing = client_rec
      .map_err(|_| "Unknown error".into(): Error)
      .for_each(clone!(log; |ev| {
         // Handle events received from the service.
         match ev {
            ClientEv::Message(msg) => {
               if !open {
                  bail!("Attempt to send message on closed stream.");
               }
               // Forward any messages to the client.
               if sink.start_send(msg)?.is_not_ready() {
                  // Handle back-pressure by simply dropping the connection.
                  bail!("Stream buffer full, dropping connection");
               }
               if let Err(e) = sink.poll_complete() {
                  bail!("Poll complete failed: {}", e);
               }
            }
            ClientEv::Shutdown => {
               open = false;
               info!(log, "Got ClientEv::Shutdown, closing sink.");
               let _ = sink.close();
            }
         }
         Ok(())
      }))
      .then(clone!(log; |res| {
         match res {
            Ok(_) => info!(log, "Sink closed ({})", id),
            Err(e) => error!(log, "Socket outgoing error: {}", e),
         }
         Ok(())
      }));

   // Return a future which resolves when both the incoming and outgoing streams
   // have completed.
   incoming.join(outgoing).map(|_| ()).boxed()
}

/// A protocol for creating discrete messages from a stream of bytes.
struct Framing {
   write_buffer: Vec<u8>,
}

impl Framing {
   /// Create a new `Framing`.
   pub fn new() -> Framing {
      Framing { write_buffer: Vec::with_capacity(512) }
   }
}

impl Encoder for Framing {
   type Item = message::Message;
   type Error = Error;
   fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<()> {
      // Write the item to our buffer.
      self.write_buffer.clear();
      encode(&item, &mut self.write_buffer)?;

      // Reserve enough space to store the message.
      dst.reserve(self.write_buffer.len() + 4);

      // Write the length, followed by the message data, to the socket's buffer.
      dst.put_u32::<BigEndian>(self.write_buffer.len() as u32);
      dst.put(&self.write_buffer);

      Ok(())
   }
}

impl Decoder for Framing {
   type Item = message::Message;
   type Error = Error;
   fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>> {
      if src.len() < 4 { return Ok(None); }

      // Ensure we have the entire message before attempting to decode it.
      let len = BigEndian::read_u32(src) as usize;
      if src.len() < len + 4 { return Ok(None) }

      // Take the length and data from the buffer.
      let _ = src.split_to(4);
      let data = src.split_to(len);

      // Parse the message
      Ok(Some(decode(&data)?))
   }
}
