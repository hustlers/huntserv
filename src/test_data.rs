use message;
use error::*;
use message::Permission;
use bcrypt;
use rand::*;
use database::Database;
use data::DataTrait;

/// The main container for all test data.
#[derive(Deserialize, Debug, Clone)]
pub struct Hunt {
   pub name: String,
   pub teams: Vec<Team>,
   pub points: Vec<Point>,
   pub demonstrators: Vec<User>,
   pub bonuses: Vec<Bonus>,
   pub location: Vec<f64>,
   pub radius: f64,
   pub random_passwords: bool,
}

/// A user
#[derive(Deserialize, Debug, Clone)]
pub struct User {
   pub name: String,
   pub username: Option<String>,
   pub password: Option<String>,
}

/// A team
#[derive(Deserialize, Debug, Clone)]
pub struct Team {
   pub name: String,
   pub players: Vec<String>,
   pub location: Vec<f64>,
}

/// A Point
#[derive(Deserialize, Debug, Clone)]
pub struct Point {
   pub clue: String,
   pub pw: String,
   pub location: Vec<f64>,
}

/// A bonus point
#[derive(Deserialize, Debug, Clone)]
pub struct Bonus {
   pub url: String,
   pub name: String,
   pub location: Vec<f64>,
   pub difficulty: u32,
   pub password: String,
}

/// Creates a location from the format used in test data.
pub fn loc(v: &[f64]) -> Result<message::Location> {
   if v.len() != 2 {
      bail!("Invalid location: {:?}, should be [lat,long]", v);
   }
   let mut loc = message::Location::new();
   loc.set_latitude(v[0]);
   loc.set_longitude(v[1]);
   Ok(loc)
}

/// Create a randomised location from a given distribution.
pub fn random_loc<S: distributions::Sample<f64>>(base: &message::Location, dist: &mut S)
   -> message::Location {
   let mut loc = message::Location::new();
   loc.set_latitude(base.get_latitude() + dist.sample(&mut thread_rng()));
   loc.set_longitude(base.get_longitude() + (dist.sample(&mut thread_rng()) * 0.5));
   loc
}

/// Generate a username from a full name
pub fn make_username(name: &str, db: Option<&Database>) -> Result<String> {
   let mut i = 1;
   loop {
      let names: Vec<_> = name.split_whitespace().map(|x| x.to_lowercase()).collect();
      let name = match names.len() {
         0 => bail!("Invalid call to make_username({})", name),
         1 => format!("{}{}", names[0], i),
         x => format!("{}{}{}", names[0].chars().nth(0).unwrap(), names[x-1], i),
      };
      if db.is_none() || !db.unwrap().has_username(&name) {
         return Ok(name);
      }
      i += 1;
   }
}

/// Generate a random 6 character alphanumeric string.
pub fn random_password() -> String {
   let chars: Vec<char> = "0123456789abcdefghijklmnopqrstuvwxyz".chars().collect();
   let len = chars.len() as u32;
   (0..6).map(|_| chars[(thread_rng().next_u32() % len) as usize])
      .collect()
}

impl Hunt {
   /// Write the hunt to the database
   pub fn to_db(&self, db: &Database, users: &mut Vec<User>) -> Result<u32> {
      let mut hunt = message::Hunt::new();
      let hunt_id = db.hunts.next_id();
      hunt.set_id(hunt_id);
      hunt.set_location(loc(&self.location)?);
      hunt.set_radius(self.radius);
      hunt.set_name(self.name.clone());

      for team in &self.teams {
         hunt.mut_teams().push(team.to_db(db, hunt_id, self.random_passwords, users)?);
      }

      for bonus in &self.bonuses {
         hunt.mut_bonuses().push(bonus.to_db(db)?);
      }

      for point in &self.points {
         hunt.mut_points().push(point.to_db(db)?);
      }

      for user in &self.demonstrators {
         let mut user = user.clone();
         let user_id = user.to_db(db)?;
         users.push(user);
         hunt.mut_demonstrators().push(user_id);
         db.users.get_mut(user_id).unwrap()
            .set_permission(Permission::DEMONSTRATOR);
      }

      hunt.insert_into(&db);
      Ok(hunt_id)
   }
}

impl Team {
   pub fn to_db(&self, db: &Database, hunt_id: u32, random_passwords: bool, users: &mut Vec<User>) -> Result<u32> {
      let mut team = message::Team::new();
      let team_id = db.teams.next_id();

      team.set_id(team_id);
      team.set_hunt(hunt_id);
      team.set_name(self.name.clone());

      let base_loc = loc(&self.location)?;
      let mut distribution = distributions::Normal::new(0.0, 0.00004);

      for name in &self.players {
         let mut user = User {
            name: name.clone(),
            username: None,
            password: if random_passwords {None} else {Some("password".to_string())},
         };
         let user_id = user.to_db(db)?;
         users.push(user);
         team.mut_players().push(user_id);

         let mut user = db.users.get_mut(user_id).unwrap();
         user.set_team_id(team_id);
         user.set_location(random_loc(&base_loc, &mut distribution));
         user.set_permission(message::Permission::PLAYER);
      }

      team.insert_into(&db);
      Ok(team_id)
   }
}

impl User {
   pub fn to_db(&mut self, db: &Database) -> Result<u32> {
      if self.username.is_none() { self.username = Some(make_username(&self.name, Some(db))?); }
      if self.password.is_none() { self.password = Some(random_password()); }
      let player_id = db.users.next_id();

      let mut user = message::User::new();
      user.set_id(player_id);
      user.set_name(self.name.clone());
      user.insert_into(&db);

      let mut login = message::LoginDetails::new();
      login.set_user_id(player_id);
      login.set_username(self.username.as_ref().unwrap().clone());
      login.set_password(bcrypt::hash(&self.password.as_ref().unwrap(), 6)?);
      login.insert_into(&db);

      Ok(player_id)
   }
}

impl Point {
   pub fn to_db(&self, db: &Database) -> Result<u32> {
      let point_id = db.points.next_id();
      let mut point = message::Point::new();
      point.set_id(point_id);
      point.set_clue(self.clue.clone());
      point.set_password(self.pw.clone());
      point.set_location(loc(&self.location)?);
      point.insert_into(&db);
      Ok(point_id)
   }
}

impl Bonus {
   pub fn to_db(&self, db: &Database) -> Result<u32> {
      let bonus_id = db.bonuses.next_id();
      let mut bonus = message::Bonus::new();
      bonus.set_id(bonus_id);
      bonus.set_name(self.name.clone());
      bonus.set_image_url(self.url.clone());
      bonus.set_location(loc(&self.location)?);
      bonus.set_difficulty(self.difficulty);
      bonus.set_password(self.password.clone());
      bonus.insert_into(&db);
      Ok(bonus_id)
   }
}

